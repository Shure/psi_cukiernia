﻿namespace Cukiernia
{
    partial class Order
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView_Order = new System.Windows.Forms.DataGridView();
            this.listView_Order = new System.Windows.Forms.ListView();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_InfoOrder = new System.Windows.Forms.TextBox();
            this.btn_SendOrder = new System.Windows.Forms.Button();
            this.btn_CloseOrder = new System.Windows.Forms.Button();
            this.txt_SurnameOrder = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btn_AddProductToOrderList = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Order)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Calligraphy", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(187, 27);
            this.label1.TabIndex = 0;
            this.label1.Text = "Lista produktów";
            // 
            // dataGridView_Order
            // 
            this.dataGridView_Order.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Order.Location = new System.Drawing.Point(41, 50);
            this.dataGridView_Order.Name = "dataGridView_Order";
            this.dataGridView_Order.Size = new System.Drawing.Size(428, 109);
            this.dataGridView_Order.TabIndex = 1;
            // 
            // listView_Order
            // 
            this.listView_Order.Location = new System.Drawing.Point(41, 165);
            this.listView_Order.Name = "listView_Order";
            this.listView_Order.Size = new System.Drawing.Size(688, 68);
            this.listView_Order.TabIndex = 2;
            this.listView_Order.UseCompatibleStateImageBehavior = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(314, 326);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(147, 17);
            this.label2.TabIndex = 3;
            this.label2.Text = "Dodatkowe informacje";
            // 
            // txt_InfoOrder
            // 
            this.txt_InfoOrder.Location = new System.Drawing.Point(467, 327);
            this.txt_InfoOrder.Name = "txt_InfoOrder";
            this.txt_InfoOrder.Size = new System.Drawing.Size(262, 20);
            this.txt_InfoOrder.TabIndex = 4;
            // 
            // btn_SendOrder
            // 
            this.btn_SendOrder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btn_SendOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SendOrder.Location = new System.Drawing.Point(467, 369);
            this.btn_SendOrder.Name = "btn_SendOrder";
            this.btn_SendOrder.Size = new System.Drawing.Size(262, 41);
            this.btn_SendOrder.TabIndex = 5;
            this.btn_SendOrder.Text = "Wyślij zapotrzebowanie";
            this.btn_SendOrder.UseVisualStyleBackColor = false;
            // 
            // btn_CloseOrder
            // 
            this.btn_CloseOrder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_CloseOrder.Font = new System.Drawing.Font("Lucida Calligraphy", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_CloseOrder.Location = new System.Drawing.Point(705, 3);
            this.btn_CloseOrder.Name = "btn_CloseOrder";
            this.btn_CloseOrder.Size = new System.Drawing.Size(92, 33);
            this.btn_CloseOrder.TabIndex = 6;
            this.btn_CloseOrder.Text = "Zamknij";
            this.btn_CloseOrder.UseVisualStyleBackColor = false;
            this.btn_CloseOrder.Click += new System.EventHandler(this.button2_Click);
            // 
            // txt_SurnameOrder
            // 
            this.txt_SurnameOrder.Location = new System.Drawing.Point(467, 306);
            this.txt_SurnameOrder.Name = "txt_SurnameOrder";
            this.txt_SurnameOrder.Size = new System.Drawing.Size(262, 20);
            this.txt_SurnameOrder.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(349, 307);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 17);
            this.label3.TabIndex = 8;
            this.label3.Text = "Nazwisko klienta";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(38, 268);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 17);
            this.label4.TabIndex = 10;
            this.label4.Text = "Data realizacji";
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(132, 271);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(576, 71);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 17);
            this.label5.TabIndex = 12;
            this.label5.Text = "Ilość";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(475, 91);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(254, 20);
            this.textBox1.TabIndex = 13;
            // 
            // btn_AddProductToOrderList
            // 
            this.btn_AddProductToOrderList.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.btn_AddProductToOrderList.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AddProductToOrderList.Location = new System.Drawing.Point(475, 111);
            this.btn_AddProductToOrderList.Name = "btn_AddProductToOrderList";
            this.btn_AddProductToOrderList.Size = new System.Drawing.Size(254, 25);
            this.btn_AddProductToOrderList.TabIndex = 14;
            this.btn_AddProductToOrderList.Text = "Dodaj produkt";
            this.btn_AddProductToOrderList.UseVisualStyleBackColor = false;
            // 
            // Order
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_AddProductToOrderList);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.monthCalendar1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txt_SurnameOrder);
            this.Controls.Add(this.btn_CloseOrder);
            this.Controls.Add(this.btn_SendOrder);
            this.Controls.Add(this.txt_InfoOrder);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.listView_Order);
            this.Controls.Add(this.dataGridView_Order);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Order";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Zapotrzebowanie";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Order)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView_Order;
        private System.Windows.Forms.ListView listView_Order;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_InfoOrder;
        private System.Windows.Forms.Button btn_SendOrder;
        private System.Windows.Forms.Button btn_CloseOrder;
        private System.Windows.Forms.TextBox txt_SurnameOrder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btn_AddProductToOrderList;
    }
}