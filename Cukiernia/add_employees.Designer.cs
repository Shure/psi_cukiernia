﻿namespace Cukiernia
{
    partial class add_employees
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.txt_addname = new System.Windows.Forms.TextBox();
            this.txt_addsurname = new System.Windows.Forms.TextBox();
            this.txt_addphone = new System.Windows.Forms.TextBox();
            this.txt_addlogin = new System.Windows.Forms.TextBox();
            this.txt_addpassw = new System.Windows.Forms.TextBox();
            this.label_name = new System.Windows.Forms.Label();
            this.label_surname = new System.Windows.Forms.Label();
            this.label_phone = new System.Windows.Forms.Label();
            this.label_login = new System.Windows.Forms.Label();
            this.label_passw = new System.Windows.Forms.Label();
            this.label_add_employees = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_save
            // 
            this.btn_save.Location = new System.Drawing.Point(479, 340);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(100, 35);
            this.btn_save.TabIndex = 0;
            this.btn_save.Text = "Zatwierdź";
            this.btn_save.UseVisualStyleBackColor = true;
            // 
            // btn_cancel
            // 
            this.btn_cancel.Location = new System.Drawing.Point(610, 340);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(100, 35);
            this.btn_cancel.TabIndex = 1;
            this.btn_cancel.Text = "Anuluj";
            this.btn_cancel.UseVisualStyleBackColor = true;
            // 
            // txt_addname
            // 
            this.txt_addname.Location = new System.Drawing.Point(116, 108);
            this.txt_addname.Name = "txt_addname";
            this.txt_addname.Size = new System.Drawing.Size(364, 20);
            this.txt_addname.TabIndex = 2;
            // 
            // txt_addsurname
            // 
            this.txt_addsurname.Location = new System.Drawing.Point(116, 149);
            this.txt_addsurname.Name = "txt_addsurname";
            this.txt_addsurname.Size = new System.Drawing.Size(364, 20);
            this.txt_addsurname.TabIndex = 3;
            // 
            // txt_addphone
            // 
            this.txt_addphone.Location = new System.Drawing.Point(116, 188);
            this.txt_addphone.Name = "txt_addphone";
            this.txt_addphone.Size = new System.Drawing.Size(364, 20);
            this.txt_addphone.TabIndex = 4;
            // 
            // txt_addlogin
            // 
            this.txt_addlogin.Location = new System.Drawing.Point(116, 252);
            this.txt_addlogin.Name = "txt_addlogin";
            this.txt_addlogin.Size = new System.Drawing.Size(364, 20);
            this.txt_addlogin.TabIndex = 5;
            // 
            // txt_addpassw
            // 
            this.txt_addpassw.Location = new System.Drawing.Point(116, 287);
            this.txt_addpassw.Name = "txt_addpassw";
            this.txt_addpassw.Size = new System.Drawing.Size(364, 20);
            this.txt_addpassw.TabIndex = 6;
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_name.Location = new System.Drawing.Point(26, 108);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(48, 20);
            this.label_name.TabIndex = 7;
            this.label_name.Text = "Imię:";
            // 
            // label_surname
            // 
            this.label_surname.AutoSize = true;
            this.label_surname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_surname.Location = new System.Drawing.Point(26, 149);
            this.label_surname.Name = "label_surname";
            this.label_surname.Size = new System.Drawing.Size(89, 20);
            this.label_surname.TabIndex = 8;
            this.label_surname.Text = "Nazwisko:";
            // 
            // label_phone
            // 
            this.label_phone.AutoSize = true;
            this.label_phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_phone.Location = new System.Drawing.Point(26, 188);
            this.label_phone.Name = "label_phone";
            this.label_phone.Size = new System.Drawing.Size(74, 20);
            this.label_phone.TabIndex = 9;
            this.label_phone.Text = "Telefon:";
            // 
            // label_login
            // 
            this.label_login.AutoSize = true;
            this.label_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_login.Location = new System.Drawing.Point(26, 252);
            this.label_login.Name = "label_login";
            this.label_login.Size = new System.Drawing.Size(58, 20);
            this.label_login.TabIndex = 10;
            this.label_login.Text = "Login:";
            // 
            // label_passw
            // 
            this.label_passw.AutoSize = true;
            this.label_passw.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_passw.Location = new System.Drawing.Point(26, 287);
            this.label_passw.Name = "label_passw";
            this.label_passw.Size = new System.Drawing.Size(61, 20);
            this.label_passw.TabIndex = 11;
            this.label_passw.Text = "Hasło:";
            // 
            // label_add_employees
            // 
            this.label_add_employees.AutoSize = true;
            this.label_add_employees.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_add_employees.Location = new System.Drawing.Point(267, 28);
            this.label_add_employees.Name = "label_add_employees";
            this.label_add_employees.Size = new System.Drawing.Size(227, 31);
            this.label_add_employees.TabIndex = 12;
            this.label_add_employees.Text = "Nowy pracownik";
            this.label_add_employees.Click += new System.EventHandler(this.label1_Click);
            // 
            // add_employees
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.CornflowerBlue;
            this.ClientSize = new System.Drawing.Size(784, 411);
            this.Controls.Add(this.label_add_employees);
            this.Controls.Add(this.label_passw);
            this.Controls.Add(this.label_login);
            this.Controls.Add(this.label_phone);
            this.Controls.Add(this.label_surname);
            this.Controls.Add(this.label_name);
            this.Controls.Add(this.txt_addpassw);
            this.Controls.Add(this.txt_addlogin);
            this.Controls.Add(this.txt_addphone);
            this.Controls.Add(this.txt_addsurname);
            this.Controls.Add(this.txt_addname);
            this.Controls.Add(this.btn_cancel);
            this.Controls.Add(this.btn_save);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "add_employees";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "add_employees";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.TextBox txt_addname;
        private System.Windows.Forms.TextBox txt_addsurname;
        private System.Windows.Forms.TextBox txt_addphone;
        private System.Windows.Forms.TextBox txt_addlogin;
        private System.Windows.Forms.TextBox txt_addpassw;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.Label label_surname;
        private System.Windows.Forms.Label label_phone;
        private System.Windows.Forms.Label label_login;
        private System.Windows.Forms.Label label_passw;
        private System.Windows.Forms.Label label_add_employees;
    }
}