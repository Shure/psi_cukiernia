﻿namespace Cukiernia
{
    partial class Confectioner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Logout = new System.Windows.Forms.Button();
            this.btn_Request = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridView_InProgressOrders = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_DataLater = new System.Windows.Forms.Button();
            this.btn_DataEalier = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.listView_ReadyOrders = new System.Windows.Forms.ListView();
            this.btn_AddToListReadyOrders = new System.Windows.Forms.Button();
            this.btn_SendReadyOrders = new System.Windows.Forms.Button();
            this.btn_ShowAllOrders = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_InProgressOrders)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Logout
            // 
            this.btn_Logout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_Logout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Logout.ForeColor = System.Drawing.Color.DarkRed;
            this.btn_Logout.Location = new System.Drawing.Point(672, 1);
            this.btn_Logout.Name = "btn_Logout";
            this.btn_Logout.Size = new System.Drawing.Size(126, 26);
            this.btn_Logout.TabIndex = 10;
            this.btn_Logout.Text = "ZAMKNIJ";
            this.btn_Logout.UseVisualStyleBackColor = false;
            this.btn_Logout.Click += new System.EventHandler(this.btn_Logout_Click);
            // 
            // btn_Request
            // 
            this.btn_Request.BackColor = System.Drawing.Color.Khaki;
            this.btn_Request.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Request.ForeColor = System.Drawing.Color.Brown;
            this.btn_Request.Location = new System.Drawing.Point(658, 163);
            this.btn_Request.Name = "btn_Request";
            this.btn_Request.Size = new System.Drawing.Size(130, 37);
            this.btn_Request.TabIndex = 13;
            this.btn_Request.Text = "ZGŁOŚ ZAPOTRZEBOWANIE";
            this.btn_Request.UseVisualStyleBackColor = false;
            this.btn_Request.Click += new System.EventHandler(this.btn_Request_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.MistyRose;
            this.panel1.Controls.Add(this.dataGridView_InProgressOrders);
            this.panel1.Location = new System.Drawing.Point(23, 28);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(629, 171);
            this.panel1.TabIndex = 14;
            // 
            // dataGridView_InProgressOrders
            // 
            this.dataGridView_InProgressOrders.AllowUserToAddRows = false;
            this.dataGridView_InProgressOrders.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_InProgressOrders.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_InProgressOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_InProgressOrders.Location = new System.Drawing.Point(3, 7);
            this.dataGridView_InProgressOrders.Name = "dataGridView_InProgressOrders";
            this.dataGridView_InProgressOrders.RowHeadersVisible = false;
            this.dataGridView_InProgressOrders.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_InProgressOrders.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_InProgressOrders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_InProgressOrders.Size = new System.Drawing.Size(623, 155);
            this.dataGridView_InProgressOrders.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Bisque;
            this.panel2.Controls.Add(this.btn_DataLater);
            this.panel2.Controls.Add(this.btn_DataEalier);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(658, 28);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(130, 136);
            this.panel2.TabIndex = 15;
            // 
            // btn_DataLater
            // 
            this.btn_DataLater.ForeColor = System.Drawing.Color.DarkRed;
            this.btn_DataLater.Location = new System.Drawing.Point(2, 76);
            this.btn_DataLater.Name = "btn_DataLater";
            this.btn_DataLater.Size = new System.Drawing.Size(125, 38);
            this.btn_DataLater.TabIndex = 23;
            this.btn_DataLater.Text = "OD NAJPÓŹNIEJSZEJ";
            this.btn_DataLater.UseVisualStyleBackColor = true;
            // 
            // btn_DataEalier
            // 
            this.btn_DataEalier.ForeColor = System.Drawing.Color.DarkRed;
            this.btn_DataEalier.Location = new System.Drawing.Point(2, 31);
            this.btn_DataEalier.Name = "btn_DataEalier";
            this.btn_DataEalier.Size = new System.Drawing.Size(126, 38);
            this.btn_DataEalier.TabIndex = 22;
            this.btn_DataEalier.Text = "OD NAJWCZEŚNIEJSZEJ";
            this.btn_DataEalier.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DarkRed;
            this.label3.Location = new System.Drawing.Point(1, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "FILTRY PO DACIE";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(19, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(232, 17);
            this.label1.TabIndex = 16;
            this.label1.Text = "Zamówienia w trakcie realizacji";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(19, 227);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(192, 17);
            this.label2.TabIndex = 17;
            this.label2.Text = "Zamówienia zrealizowane";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.MistyRose;
            this.panel3.Controls.Add(this.listView_ReadyOrders);
            this.panel3.Location = new System.Drawing.Point(23, 248);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(629, 171);
            this.panel3.TabIndex = 15;
            // 
            // listView_ReadyOrders
            // 
            this.listView_ReadyOrders.Location = new System.Drawing.Point(3, 8);
            this.listView_ReadyOrders.Name = "listView_ReadyOrders";
            this.listView_ReadyOrders.Size = new System.Drawing.Size(623, 154);
            this.listView_ReadyOrders.TabIndex = 0;
            this.listView_ReadyOrders.UseCompatibleStateImageBehavior = false;
            // 
            // btn_AddToListReadyOrders
            // 
            this.btn_AddToListReadyOrders.BackColor = System.Drawing.Color.Yellow;
            this.btn_AddToListReadyOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AddToListReadyOrders.ForeColor = System.Drawing.Color.Brown;
            this.btn_AddToListReadyOrders.Location = new System.Drawing.Point(408, 199);
            this.btn_AddToListReadyOrders.Name = "btn_AddToListReadyOrders";
            this.btn_AddToListReadyOrders.Size = new System.Drawing.Size(245, 37);
            this.btn_AddToListReadyOrders.TabIndex = 18;
            this.btn_AddToListReadyOrders.Text = "DODAJ DO LISTY ZREALIZOWANYCH";
            this.btn_AddToListReadyOrders.UseVisualStyleBackColor = false;
            // 
            // btn_SendReadyOrders
            // 
            this.btn_SendReadyOrders.BackColor = System.Drawing.Color.Lime;
            this.btn_SendReadyOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 6F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_SendReadyOrders.ForeColor = System.Drawing.Color.Brown;
            this.btn_SendReadyOrders.Location = new System.Drawing.Point(658, 374);
            this.btn_SendReadyOrders.Name = "btn_SendReadyOrders";
            this.btn_SendReadyOrders.Size = new System.Drawing.Size(130, 46);
            this.btn_SendReadyOrders.TabIndex = 19;
            this.btn_SendReadyOrders.Text = "WYŚLIJ LISTĘ ZREALIZOWANYCH ZAMÓWIEŃ";
            this.btn_SendReadyOrders.UseVisualStyleBackColor = false;
            // 
            // btn_ShowAllOrders
            // 
            this.btn_ShowAllOrders.BackColor = System.Drawing.Color.Cyan;
            this.btn_ShowAllOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_ShowAllOrders.ForeColor = System.Drawing.Color.Brown;
            this.btn_ShowAllOrders.Location = new System.Drawing.Point(452, 419);
            this.btn_ShowAllOrders.Name = "btn_ShowAllOrders";
            this.btn_ShowAllOrders.Size = new System.Drawing.Size(335, 31);
            this.btn_ShowAllOrders.TabIndex = 20;
            this.btn_ShowAllOrders.Text = "WSZYSTKIE ZAMÓWIENIA";
            this.btn_ShowAllOrders.UseVisualStyleBackColor = false;
            this.btn_ShowAllOrders.Click += new System.EventHandler(this.btn_ShowAllOrders_Click);
            // 
            // Confectioner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSalmon;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_ShowAllOrders);
            this.Controls.Add(this.btn_SendReadyOrders);
            this.Controls.Add(this.btn_AddToListReadyOrders);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_Request);
            this.Controls.Add(this.btn_Logout);
            this.ForeColor = System.Drawing.Color.LightGreen;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Confectioner";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Confecttioner";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_InProgressOrders)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_Logout;
        private System.Windows.Forms.Button btn_Request;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btn_AddToListReadyOrders;
        private System.Windows.Forms.Button btn_SendReadyOrders;
        private System.Windows.Forms.DataGridView dataGridView_InProgressOrders;
        private System.Windows.Forms.ListView listView_ReadyOrders;
        private System.Windows.Forms.Button btn_ShowAllOrders;
        private System.Windows.Forms.Button btn_DataLater;
        private System.Windows.Forms.Button btn_DataEalier;
        private System.Windows.Forms.Label label3;
    }
}