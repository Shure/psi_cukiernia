﻿namespace Cukiernia
{
    partial class Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_DailyReport = new System.Windows.Forms.Button();
            this.btn_WeeklyReport = new System.Windows.Forms.Button();
            this.btn_MonthlyReport = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_Logout = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_DailyReport
            // 
            this.btn_DailyReport.BackColor = System.Drawing.Color.Khaki;
            this.btn_DailyReport.Font = new System.Drawing.Font("Lucida Calligraphy", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DailyReport.Location = new System.Drawing.Point(12, 12);
            this.btn_DailyReport.Name = "btn_DailyReport";
            this.btn_DailyReport.Size = new System.Drawing.Size(89, 37);
            this.btn_DailyReport.TabIndex = 15;
            this.btn_DailyReport.Text = "DZIENNY";
            this.btn_DailyReport.UseVisualStyleBackColor = false;
            // 
            // btn_WeeklyReport
            // 
            this.btn_WeeklyReport.BackColor = System.Drawing.Color.Khaki;
            this.btn_WeeklyReport.Font = new System.Drawing.Font("Lucida Calligraphy", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_WeeklyReport.Location = new System.Drawing.Point(107, 12);
            this.btn_WeeklyReport.Name = "btn_WeeklyReport";
            this.btn_WeeklyReport.Size = new System.Drawing.Size(125, 37);
            this.btn_WeeklyReport.TabIndex = 16;
            this.btn_WeeklyReport.Text = "TYGODNIOWY";
            this.btn_WeeklyReport.UseVisualStyleBackColor = false;
            // 
            // btn_MonthlyReport
            // 
            this.btn_MonthlyReport.BackColor = System.Drawing.Color.Khaki;
            this.btn_MonthlyReport.Font = new System.Drawing.Font("Lucida Calligraphy", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_MonthlyReport.Location = new System.Drawing.Point(238, 12);
            this.btn_MonthlyReport.Name = "btn_MonthlyReport";
            this.btn_MonthlyReport.Size = new System.Drawing.Size(108, 37);
            this.btn_MonthlyReport.TabIndex = 17;
            this.btn_MonthlyReport.Text = "MIESIĘCZNY";
            this.btn_MonthlyReport.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Wheat;
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Location = new System.Drawing.Point(12, 65);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(776, 312);
            this.panel1.TabIndex = 18;
            // 
            // btn_Logout
            // 
            this.btn_Logout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_Logout.Font = new System.Drawing.Font("Lucida Calligraphy", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Logout.ForeColor = System.Drawing.Color.DarkRed;
            this.btn_Logout.Location = new System.Drawing.Point(664, 4);
            this.btn_Logout.Name = "btn_Logout";
            this.btn_Logout.Size = new System.Drawing.Size(126, 26);
            this.btn_Logout.TabIndex = 19;
            this.btn_Logout.Text = "ZAMKNIJ";
            this.btn_Logout.UseVisualStyleBackColor = false;
            this.btn_Logout.Click += new System.EventHandler(this.btn_Logout_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(549, 383);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(239, 20);
            this.textBox1.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Calligraphy", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(490, 385);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 17);
            this.label1.TabIndex = 21;
            this.label1.Text = "Kwota";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(16, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(746, 288);
            this.dataGridView1.TabIndex = 0;
            // 
            // Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gold;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btn_Logout);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_MonthlyReport);
            this.Controls.Add(this.btn_WeeklyReport);
            this.Controls.Add(this.btn_DailyReport);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Report";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Report";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_DailyReport;
        private System.Windows.Forms.Button btn_WeeklyReport;
        private System.Windows.Forms.Button btn_MonthlyReport;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_Logout;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}