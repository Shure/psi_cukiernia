﻿namespace Cukiernia
{
    partial class AllOrders
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_Close = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_ShowSurname = new System.Windows.Forms.TextBox();
            this.textBox_ShowProduct = new System.Windows.Forms.TextBox();
            this.btn_Ready = new System.Windows.Forms.Button();
            this.btn_InProgress = new System.Windows.Forms.Button();
            this.btn_DataLater = new System.Windows.Forms.Button();
            this.btn_DataEalier = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView_AllOrders = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_AllOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkTurquoise;
            this.panel1.Controls.Add(this.btn_Close);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.textBox_ShowSurname);
            this.panel1.Controls.Add(this.textBox_ShowProduct);
            this.panel1.Controls.Add(this.btn_Ready);
            this.panel1.Controls.Add(this.btn_InProgress);
            this.panel1.Controls.Add(this.btn_DataLater);
            this.panel1.Controls.Add(this.btn_DataEalier);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(776, 108);
            this.panel1.TabIndex = 0;
            // 
            // btn_Close
            // 
            this.btn_Close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Close.Location = new System.Drawing.Point(684, 0);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(92, 33);
            this.btn_Close.TabIndex = 10;
            this.btn_Close.Text = "Zamknij";
            this.btn_Close.UseVisualStyleBackColor = false;
            this.btn_Close.Click += new System.EventHandler(this.btn_Close_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(626, 45);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 17);
            this.label5.TabIndex = 9;
            this.label5.Text = "NAZWISKO";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(430, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "PRODUKT";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(228, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 17);
            this.label3.TabIndex = 7;
            this.label3.Text = "STATUS";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(57, 23);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "DATA";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(1, 1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "FILTRY";
            // 
            // textBox_ShowSurname
            // 
            this.textBox_ShowSurname.Location = new System.Drawing.Point(601, 70);
            this.textBox_ShowSurname.Name = "textBox_ShowSurname";
            this.textBox_ShowSurname.Size = new System.Drawing.Size(149, 20);
            this.textBox_ShowSurname.TabIndex = 4;
            // 
            // textBox_ShowProduct
            // 
            this.textBox_ShowProduct.Location = new System.Drawing.Point(375, 71);
            this.textBox_ShowProduct.Name = "textBox_ShowProduct";
            this.textBox_ShowProduct.Size = new System.Drawing.Size(207, 20);
            this.textBox_ShowProduct.TabIndex = 3;
            // 
            // btn_Ready
            // 
            this.btn_Ready.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Ready.Location = new System.Drawing.Point(189, 70);
            this.btn_Ready.Name = "btn_Ready";
            this.btn_Ready.Size = new System.Drawing.Size(150, 23);
            this.btn_Ready.TabIndex = 2;
            this.btn_Ready.Text = "GOTOWE";
            this.btn_Ready.UseVisualStyleBackColor = true;
            // 
            // btn_InProgress
            // 
            this.btn_InProgress.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_InProgress.Location = new System.Drawing.Point(189, 41);
            this.btn_InProgress.Name = "btn_InProgress";
            this.btn_InProgress.Size = new System.Drawing.Size(150, 23);
            this.btn_InProgress.TabIndex = 1;
            this.btn_InProgress.Text = "W TRAKCIE";
            this.btn_InProgress.UseVisualStyleBackColor = true;
            // 
            // btn_DataLater
            // 
            this.btn_DataLater.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DataLater.Location = new System.Drawing.Point(12, 70);
            this.btn_DataLater.Name = "btn_DataLater";
            this.btn_DataLater.Size = new System.Drawing.Size(150, 23);
            this.btn_DataLater.TabIndex = 1;
            this.btn_DataLater.Text = "OD PÓŹNIEJSZEJ";
            this.btn_DataLater.UseVisualStyleBackColor = true;
            // 
            // btn_DataEalier
            // 
            this.btn_DataEalier.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DataEalier.Location = new System.Drawing.Point(12, 41);
            this.btn_DataEalier.Name = "btn_DataEalier";
            this.btn_DataEalier.Size = new System.Drawing.Size(150, 23);
            this.btn_DataEalier.TabIndex = 0;
            this.btn_DataEalier.Text = "OD WCZEŚNIEJSZEJ";
            this.btn_DataEalier.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.PaleTurquoise;
            this.panel2.Controls.Add(this.dataGridView_AllOrders);
            this.panel2.Location = new System.Drawing.Point(12, 126);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(776, 312);
            this.panel2.TabIndex = 1;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // dataGridView_AllOrders
            // 
            this.dataGridView_AllOrders.AllowUserToAddRows = false;
            this.dataGridView_AllOrders.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_AllOrders.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_AllOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_AllOrders.Location = new System.Drawing.Point(12, 13);
            this.dataGridView_AllOrders.Name = "dataGridView_AllOrders";
            this.dataGridView_AllOrders.RowHeadersVisible = false;
            this.dataGridView_AllOrders.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_AllOrders.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_AllOrders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_AllOrders.Size = new System.Drawing.Size(748, 286);
            this.dataGridView_AllOrders.TabIndex = 0;
            // 
            // AllOrders
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "AllOrders";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "AllOrders";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_AllOrders)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_ShowSurname;
        private System.Windows.Forms.TextBox textBox_ShowProduct;
        private System.Windows.Forms.Button btn_Ready;
        private System.Windows.Forms.Button btn_InProgress;
        private System.Windows.Forms.Button btn_DataLater;
        private System.Windows.Forms.Button btn_DataEalier;
        private System.Windows.Forms.DataGridView dataGridView_AllOrders;
        private System.Windows.Forms.Button btn_Close;
    }
}