﻿namespace Cukiernia
{
    partial class edit_employees
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label_name = new System.Windows.Forms.Label();
            this.txt_addname = new System.Windows.Forms.TextBox();
            this.label_surname = new System.Windows.Forms.Label();
            this.txt_addsurname = new System.Windows.Forms.TextBox();
            this.label_phone = new System.Windows.Forms.Label();
            this.txt_addphone = new System.Windows.Forms.TextBox();
            this.label_login = new System.Windows.Forms.Label();
            this.label_passw = new System.Windows.Forms.Label();
            this.txt_addlogin = new System.Windows.Forms.TextBox();
            this.txt_addpassw = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(194, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(360, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Edycja danych pracownika";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(523, 342);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 35);
            this.button1.TabIndex = 1;
            this.button1.Text = "Zatwierdź";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(648, 342);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 35);
            this.button2.TabIndex = 2;
            this.button2.Text = "Anuluj";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_name.Location = new System.Drawing.Point(33, 97);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(48, 20);
            this.label_name.TabIndex = 8;
            this.label_name.Text = "Imię:";
            // 
            // txt_addname
            // 
            this.txt_addname.Location = new System.Drawing.Point(128, 99);
            this.txt_addname.Name = "txt_addname";
            this.txt_addname.Size = new System.Drawing.Size(364, 20);
            this.txt_addname.TabIndex = 9;
            // 
            // label_surname
            // 
            this.label_surname.AutoSize = true;
            this.label_surname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_surname.Location = new System.Drawing.Point(33, 138);
            this.label_surname.Name = "label_surname";
            this.label_surname.Size = new System.Drawing.Size(89, 20);
            this.label_surname.TabIndex = 10;
            this.label_surname.Text = "Nazwisko:";
            // 
            // txt_addsurname
            // 
            this.txt_addsurname.Location = new System.Drawing.Point(128, 140);
            this.txt_addsurname.Name = "txt_addsurname";
            this.txt_addsurname.Size = new System.Drawing.Size(364, 20);
            this.txt_addsurname.TabIndex = 11;
            // 
            // label_phone
            // 
            this.label_phone.AutoSize = true;
            this.label_phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_phone.Location = new System.Drawing.Point(33, 182);
            this.label_phone.Name = "label_phone";
            this.label_phone.Size = new System.Drawing.Size(74, 20);
            this.label_phone.TabIndex = 12;
            this.label_phone.Text = "Telefon:";
            // 
            // txt_addphone
            // 
            this.txt_addphone.Location = new System.Drawing.Point(128, 184);
            this.txt_addphone.Name = "txt_addphone";
            this.txt_addphone.Size = new System.Drawing.Size(364, 20);
            this.txt_addphone.TabIndex = 13;
            // 
            // label_login
            // 
            this.label_login.AutoSize = true;
            this.label_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_login.Location = new System.Drawing.Point(33, 254);
            this.label_login.Name = "label_login";
            this.label_login.Size = new System.Drawing.Size(58, 20);
            this.label_login.TabIndex = 14;
            this.label_login.Text = "Login:";
            // 
            // label_passw
            // 
            this.label_passw.AutoSize = true;
            this.label_passw.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_passw.Location = new System.Drawing.Point(33, 293);
            this.label_passw.Name = "label_passw";
            this.label_passw.Size = new System.Drawing.Size(61, 20);
            this.label_passw.TabIndex = 15;
            this.label_passw.Text = "Hasło:";
            // 
            // txt_addlogin
            // 
            this.txt_addlogin.Location = new System.Drawing.Point(128, 256);
            this.txt_addlogin.Name = "txt_addlogin";
            this.txt_addlogin.Size = new System.Drawing.Size(364, 20);
            this.txt_addlogin.TabIndex = 16;
            // 
            // txt_addpassw
            // 
            this.txt_addpassw.Location = new System.Drawing.Point(128, 295);
            this.txt_addpassw.Name = "txt_addpassw";
            this.txt_addpassw.Size = new System.Drawing.Size(364, 20);
            this.txt_addpassw.TabIndex = 17;
            // 
            // edit_employees
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.CornflowerBlue;
            this.ClientSize = new System.Drawing.Size(784, 411);
            this.Controls.Add(this.txt_addpassw);
            this.Controls.Add(this.txt_addlogin);
            this.Controls.Add(this.label_passw);
            this.Controls.Add(this.label_login);
            this.Controls.Add(this.txt_addphone);
            this.Controls.Add(this.label_phone);
            this.Controls.Add(this.txt_addsurname);
            this.Controls.Add(this.label_surname);
            this.Controls.Add(this.txt_addname);
            this.Controls.Add(this.label_name);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "edit_employees";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "edit_employees";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.TextBox txt_addname;
        private System.Windows.Forms.Label label_surname;
        private System.Windows.Forms.TextBox txt_addsurname;
        private System.Windows.Forms.Label label_phone;
        private System.Windows.Forms.TextBox txt_addphone;
        private System.Windows.Forms.Label label_login;
        private System.Windows.Forms.Label label_passw;
        private System.Windows.Forms.TextBox txt_addlogin;
        private System.Windows.Forms.TextBox txt_addpassw;
    }
}