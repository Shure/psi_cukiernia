﻿namespace Cukiernia
{
    partial class employees
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_add_employees = new System.Windows.Forms.Button();
            this.btn_delete_employees = new System.Windows.Forms.Button();
            this.btn_edit_employees = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.panel1.Location = new System.Drawing.Point(22, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(508, 364);
            this.panel1.TabIndex = 0;
            // 
            // btn_add_employees
            // 
            this.btn_add_employees.Location = new System.Drawing.Point(569, 32);
            this.btn_add_employees.Name = "btn_add_employees";
            this.btn_add_employees.Size = new System.Drawing.Size(170, 40);
            this.btn_add_employees.TabIndex = 1;
            this.btn_add_employees.Text = "Dodaj pracownika";
            this.btn_add_employees.UseVisualStyleBackColor = true;
            // 
            // btn_delete_employees
            // 
            this.btn_delete_employees.Location = new System.Drawing.Point(569, 105);
            this.btn_delete_employees.Name = "btn_delete_employees";
            this.btn_delete_employees.Size = new System.Drawing.Size(170, 40);
            this.btn_delete_employees.TabIndex = 2;
            this.btn_delete_employees.Text = "Usuń pozycję";
            this.btn_delete_employees.UseVisualStyleBackColor = true;
            // 
            // btn_edit_employees
            // 
            this.btn_edit_employees.Location = new System.Drawing.Point(569, 180);
            this.btn_edit_employees.Name = "btn_edit_employees";
            this.btn_edit_employees.Size = new System.Drawing.Size(170, 40);
            this.btn_edit_employees.TabIndex = 3;
            this.btn_edit_employees.Text = "Edytuj pozycję";
            this.btn_edit_employees.UseVisualStyleBackColor = true;
            // 
            // employees
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.CornflowerBlue;
            this.ClientSize = new System.Drawing.Size(780, 400);
            this.Controls.Add(this.btn_edit_employees);
            this.Controls.Add(this.btn_delete_employees);
            this.Controls.Add(this.btn_add_employees);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "employees";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "employees";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_add_employees;
        private System.Windows.Forms.Button btn_delete_employees;
        private System.Windows.Forms.Button btn_edit_employees;
    }
}