﻿namespace Cukiernia
{
    partial class SellerPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SellerPanel));
            this.btn_Logout = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_Cakes = new System.Windows.Forms.Button();
            this.btn_Cookies = new System.Windows.Forms.Button();
            this.btn_Twirl = new System.Windows.Forms.Button();
            this.btn_Cake = new System.Windows.Forms.Button();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_Order = new System.Windows.Forms.Button();
            this.btn_Invoice = new System.Windows.Forms.Button();
            this.btn_DailyReport = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView_listOfProducts = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_EditProductReceipt = new System.Windows.Forms.Button();
            this.btn_DeleteProductReceipt = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_SumOfReceipt = new System.Windows.Forms.TextBox();
            this.listBox_Receipt = new System.Windows.Forms.ListBox();
            this.btn_addToList = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btn_CloseReceipt = new System.Windows.Forms.Button();
            this.btn_AllOrders = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_listOfProducts)).BeginInit();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Logout
            // 
            this.btn_Logout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.btn_Logout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Logout.ForeColor = System.Drawing.Color.DarkRed;
            this.btn_Logout.Location = new System.Drawing.Point(672, 2);
            this.btn_Logout.Name = "btn_Logout";
            this.btn_Logout.Size = new System.Drawing.Size(126, 26);
            this.btn_Logout.TabIndex = 9;
            this.btn_Logout.Text = "ZAMKNIJ";
            this.btn_Logout.UseVisualStyleBackColor = false;
            this.btn_Logout.Click += new System.EventHandler(this.btn_Logout_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.YellowGreen;
            this.panel1.Controls.Add(this.btn_Cakes);
            this.panel1.Controls.Add(this.btn_Cookies);
            this.panel1.Controls.Add(this.btn_Twirl);
            this.panel1.Controls.Add(this.btn_Cake);
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(415, 196);
            this.panel1.TabIndex = 10;
            // 
            // btn_Cakes
            // 
            this.btn_Cakes.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Cakes.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_Cakes.Location = new System.Drawing.Point(82, 152);
            this.btn_Cakes.Name = "btn_Cakes";
            this.btn_Cakes.Size = new System.Drawing.Size(75, 23);
            this.btn_Cakes.TabIndex = 8;
            this.btn_Cakes.Text = "CIASTA";
            this.btn_Cakes.UseVisualStyleBackColor = false;
            this.btn_Cakes.Click += new System.EventHandler(this.btn_Cakes_Click);
            // 
            // btn_Cookies
            // 
            this.btn_Cookies.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Cookies.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_Cookies.Location = new System.Drawing.Point(261, 152);
            this.btn_Cookies.Name = "btn_Cookies";
            this.btn_Cookies.Size = new System.Drawing.Size(75, 23);
            this.btn_Cookies.TabIndex = 7;
            this.btn_Cookies.Text = "CIASTKA";
            this.btn_Cookies.UseVisualStyleBackColor = false;
            this.btn_Cookies.Click += new System.EventHandler(this.btn_Cookies_Click);
            // 
            // btn_Twirl
            // 
            this.btn_Twirl.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Twirl.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_Twirl.Location = new System.Drawing.Point(261, 68);
            this.btn_Twirl.Name = "btn_Twirl";
            this.btn_Twirl.Size = new System.Drawing.Size(75, 23);
            this.btn_Twirl.TabIndex = 6;
            this.btn_Twirl.Text = "ROGALE";
            this.btn_Twirl.UseVisualStyleBackColor = false;
            this.btn_Twirl.Click += new System.EventHandler(this.btn_Twirl_Click);
            // 
            // btn_Cake
            // 
            this.btn_Cake.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Cake.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_Cake.Location = new System.Drawing.Point(82, 68);
            this.btn_Cake.Name = "btn_Cake";
            this.btn_Cake.Size = new System.Drawing.Size(75, 23);
            this.btn_Cake.TabIndex = 5;
            this.btn_Cake.Text = "TORTY";
            this.btn_Cake.UseVisualStyleBackColor = false;
            this.btn_Cake.Click += new System.EventHandler(this.btn_Cake_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(224, 103);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(153, 53);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 4;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(224, 21);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(153, 51);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(43, 103);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(153, 53);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(43, 21);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(153, 51);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(60, 216);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(257, 31);
            this.label1.TabIndex = 11;
            this.label1.Text = "Cukiernia ROGALIK";
            // 
            // btn_Order
            // 
            this.btn_Order.BackColor = System.Drawing.Color.Khaki;
            this.btn_Order.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Order.Location = new System.Drawing.Point(82, 253);
            this.btn_Order.Name = "btn_Order";
            this.btn_Order.Size = new System.Drawing.Size(277, 37);
            this.btn_Order.TabIndex = 12;
            this.btn_Order.Text = "ZŁÓŻ ZAMÓWIENIE";
            this.btn_Order.UseVisualStyleBackColor = false;
            this.btn_Order.Click += new System.EventHandler(this.btn_Order_Click);
            // 
            // btn_Invoice
            // 
            this.btn_Invoice.BackColor = System.Drawing.Color.Khaki;
            this.btn_Invoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Invoice.Location = new System.Drawing.Point(82, 292);
            this.btn_Invoice.Name = "btn_Invoice";
            this.btn_Invoice.Size = new System.Drawing.Size(277, 37);
            this.btn_Invoice.TabIndex = 13;
            this.btn_Invoice.Text = "FAKTURA";
            this.btn_Invoice.UseVisualStyleBackColor = false;
            this.btn_Invoice.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn_DailyReport
            // 
            this.btn_DailyReport.BackColor = System.Drawing.Color.Khaki;
            this.btn_DailyReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DailyReport.Location = new System.Drawing.Point(82, 401);
            this.btn_DailyReport.Name = "btn_DailyReport";
            this.btn_DailyReport.Size = new System.Drawing.Size(277, 37);
            this.btn_DailyReport.TabIndex = 14;
            this.btn_DailyReport.Text = "RAPORT";
            this.btn_DailyReport.UseVisualStyleBackColor = false;
            this.btn_DailyReport.Click += new System.EventHandler(this.btn_DailyReport_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(88, 371);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(217, 25);
            this.label2.TabIndex = 17;
            this.label2.Text = "RAPORT SPRZEDAŻY";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.YellowGreen;
            this.panel2.Controls.Add(this.dataGridView_listOfProducts);
            this.panel2.Location = new System.Drawing.Point(454, 34);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(334, 174);
            this.panel2.TabIndex = 18;
            // 
            // dataGridView_listOfProducts
            // 
            this.dataGridView_listOfProducts.AllowUserToAddRows = false;
            this.dataGridView_listOfProducts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_listOfProducts.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_listOfProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_listOfProducts.Location = new System.Drawing.Point(3, 3);
            this.dataGridView_listOfProducts.Name = "dataGridView_listOfProducts";
            this.dataGridView_listOfProducts.RowHeadersVisible = false;
            this.dataGridView_listOfProducts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_listOfProducts.Size = new System.Drawing.Size(328, 168);
            this.dataGridView_listOfProducts.TabIndex = 0;
            this.dataGridView_listOfProducts.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.MediumSpringGreen;
            this.panel3.Controls.Add(this.btn_EditProductReceipt);
            this.panel3.Controls.Add(this.btn_DeleteProductReceipt);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.txt_SumOfReceipt);
            this.panel3.Controls.Add(this.listBox_Receipt);
            this.panel3.Controls.Add(this.btn_addToList);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.textBox4);
            this.panel3.Controls.Add(this.textBox3);
            this.panel3.Controls.Add(this.textBox2);
            this.panel3.Controls.Add(this.textBox1);
            this.panel3.Location = new System.Drawing.Point(454, 217);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(331, 201);
            this.panel3.TabIndex = 19;
            // 
            // btn_EditProductReceipt
            // 
            this.btn_EditProductReceipt.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btn_EditProductReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_EditProductReceipt.Location = new System.Drawing.Point(212, 149);
            this.btn_EditProductReceipt.Name = "btn_EditProductReceipt";
            this.btn_EditProductReceipt.Size = new System.Drawing.Size(58, 23);
            this.btn_EditProductReceipt.TabIndex = 12;
            this.btn_EditProductReceipt.Text = "Edytuj";
            this.btn_EditProductReceipt.UseVisualStyleBackColor = false;
            // 
            // btn_DeleteProductReceipt
            // 
            this.btn_DeleteProductReceipt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_DeleteProductReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_DeleteProductReceipt.Location = new System.Drawing.Point(272, 149);
            this.btn_DeleteProductReceipt.Name = "btn_DeleteProductReceipt";
            this.btn_DeleteProductReceipt.Size = new System.Drawing.Size(58, 23);
            this.btn_DeleteProductReceipt.TabIndex = 11;
            this.btn_DeleteProductReceipt.Text = "Usuń";
            this.btn_DeleteProductReceipt.UseVisualStyleBackColor = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label7.Location = new System.Drawing.Point(23, 179);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(51, 17);
            this.label7.TabIndex = 10;
            this.label7.Text = "SUMA";
            // 
            // txt_SumOfReceipt
            // 
            this.txt_SumOfReceipt.BackColor = System.Drawing.Color.Red;
            this.txt_SumOfReceipt.ForeColor = System.Drawing.SystemColors.Window;
            this.txt_SumOfReceipt.Location = new System.Drawing.Point(108, 178);
            this.txt_SumOfReceipt.Name = "txt_SumOfReceipt";
            this.txt_SumOfReceipt.Size = new System.Drawing.Size(220, 20);
            this.txt_SumOfReceipt.TabIndex = 9;
            // 
            // listBox_Receipt
            // 
            this.listBox_Receipt.FormattingEnabled = true;
            this.listBox_Receipt.Location = new System.Drawing.Point(2, 66);
            this.listBox_Receipt.Name = "listBox_Receipt";
            this.listBox_Receipt.Size = new System.Drawing.Size(328, 82);
            this.listBox_Receipt.TabIndex = 9;
            // 
            // btn_addToList
            // 
            this.btn_addToList.BackColor = System.Drawing.Color.Tomato;
            this.btn_addToList.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_addToList.Location = new System.Drawing.Point(5, 42);
            this.btn_addToList.Name = "btn_addToList";
            this.btn_addToList.Size = new System.Drawing.Size(321, 23);
            this.btn_addToList.TabIndex = 8;
            this.btn_addToList.Text = "DODAJ DO ZAMÓWIENIA";
            this.btn_addToList.UseVisualStyleBackColor = false;
            this.btn_addToList.Click += new System.EventHandler(this.btn_addToList_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(252, 1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "DO ZAPŁATY";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(185, 2);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(43, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "RABAT";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(107, 2);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "ILOŚĆ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "WARTOŚĆ";
            // 
            // textBox4
            // 
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(250, 15);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(76, 20);
            this.textBox4.TabIndex = 3;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(168, 15);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(76, 20);
            this.textBox3.TabIndex = 2;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(86, 15);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(76, 20);
            this.textBox2.TabIndex = 1;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(5, 15);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(76, 20);
            this.textBox1.TabIndex = 0;
            // 
            // btn_CloseReceipt
            // 
            this.btn_CloseReceipt.BackColor = System.Drawing.Color.Gold;
            this.btn_CloseReceipt.Location = new System.Drawing.Point(540, 420);
            this.btn_CloseReceipt.Name = "btn_CloseReceipt";
            this.btn_CloseReceipt.Size = new System.Drawing.Size(165, 23);
            this.btn_CloseReceipt.TabIndex = 20;
            this.btn_CloseReceipt.Text = "ZAKOŃCZ ZAMÓWIENIE";
            this.btn_CloseReceipt.UseVisualStyleBackColor = false;
            this.btn_CloseReceipt.Click += new System.EventHandler(this.btn_CloseReceipt_Click);
            // 
            // btn_AllOrders
            // 
            this.btn_AllOrders.BackColor = System.Drawing.Color.Khaki;
            this.btn_AllOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AllOrders.Location = new System.Drawing.Point(82, 331);
            this.btn_AllOrders.Name = "btn_AllOrders";
            this.btn_AllOrders.Size = new System.Drawing.Size(277, 37);
            this.btn_AllOrders.TabIndex = 21;
            this.btn_AllOrders.Text = "WSZYSTKIE ZAMÓWIENIA";
            this.btn_AllOrders.UseVisualStyleBackColor = false;
            this.btn_AllOrders.Click += new System.EventHandler(this.btn_AllOrders_Click);
            // 
            // SellerPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btn_AllOrders);
            this.Controls.Add(this.btn_CloseReceipt);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btn_DailyReport);
            this.Controls.Add(this.btn_Invoice);
            this.Controls.Add(this.btn_Order);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_Logout);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SellerPanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SellerPanel";
            this.Load += new System.EventHandler(this.SellerPanel_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_listOfProducts)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_Logout;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_Order;
        private System.Windows.Forms.Button btn_Invoice;
        private System.Windows.Forms.Button btn_DailyReport;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_Cakes;
        private System.Windows.Forms.Button btn_Cookies;
        private System.Windows.Forms.Button btn_Twirl;
        private System.Windows.Forms.Button btn_Cake;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView_listOfProducts;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_addToList;
        private System.Windows.Forms.TextBox txt_SumOfReceipt;
        private System.Windows.Forms.ListBox listBox_Receipt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_CloseReceipt;
        private System.Windows.Forms.Button btn_AllOrders;
        private System.Windows.Forms.Button btn_EditProductReceipt;
        private System.Windows.Forms.Button btn_DeleteProductReceipt;
    }
}
