﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cukiernia
{
    public partial class supervisor : Form
    {
        public supervisor()
        {
            InitializeComponent();
        }

        private void supervisor_Load(object sender, EventArgs e)
        {

        }

        private void btn_invoices_Click(object sender, EventArgs e)
        {
            Invoice invoice = new Invoice();
            invoice.Show();
        }

        private void btn_request_Click(object sender, EventArgs e)
        {

        }

        private void btn_employees_Click(object sender, EventArgs e)
        {
            employees empl = new employees();
            empl.Show();
        }

        private void btn_orders_Click(object sender, EventArgs e)
        {
            assortment_order assort = new assortment_order();
            assort.Show();
        }

        private void btn_raports_Click(object sender, EventArgs e)
        {

        }
    }
}
