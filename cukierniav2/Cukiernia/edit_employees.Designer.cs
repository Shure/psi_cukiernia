﻿namespace Cukiernia
{
    partial class edit_employees
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label_name = new System.Windows.Forms.Label();
            this.txt_addname = new System.Windows.Forms.TextBox();
            this.label_surname = new System.Windows.Forms.Label();
            this.txt_addsurname = new System.Windows.Forms.TextBox();
            this.label_phone = new System.Windows.Forms.Label();
            this.txt_addphone = new System.Windows.Forms.TextBox();
            this.label_login = new System.Windows.Forms.Label();
            this.label_passw = new System.Windows.Forms.Label();
            this.txt_addlogin = new System.Windows.Forms.TextBox();
            this.txt_addpassw = new System.Windows.Forms.TextBox();
            this.btn_Close = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(326, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(445, 39);
            this.label1.TabIndex = 0;
            this.label1.Text = "Edycja danych pracownika";
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Silver;
            this.button1.Location = new System.Drawing.Point(302, 374);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(133, 43);
            this.button1.TabIndex = 1;
            this.button1.Text = "Zatwierdź";
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Silver;
            this.button2.Location = new System.Drawing.Point(653, 374);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(133, 43);
            this.button2.TabIndex = 2;
            this.button2.Text = "Anuluj";
            this.button2.UseVisualStyleBackColor = false;
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_name.Location = new System.Drawing.Point(25, 106);
            this.label_name.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(59, 25);
            this.label_name.TabIndex = 8;
            this.label_name.Text = "Imię:";
            // 
            // txt_addname
            // 
            this.txt_addname.Location = new System.Drawing.Point(302, 109);
            this.txt_addname.Margin = new System.Windows.Forms.Padding(4);
            this.txt_addname.Name = "txt_addname";
            this.txt_addname.Size = new System.Drawing.Size(484, 22);
            this.txt_addname.TabIndex = 9;
            this.txt_addname.TextChanged += new System.EventHandler(this.txt_addname_TextChanged);
            // 
            // label_surname
            // 
            this.label_surname.AutoSize = true;
            this.label_surname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_surname.Location = new System.Drawing.Point(25, 157);
            this.label_surname.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_surname.Name = "label_surname";
            this.label_surname.Size = new System.Drawing.Size(111, 25);
            this.label_surname.TabIndex = 10;
            this.label_surname.Text = "Nazwisko:";
            // 
            // txt_addsurname
            // 
            this.txt_addsurname.Location = new System.Drawing.Point(302, 157);
            this.txt_addsurname.Margin = new System.Windows.Forms.Padding(4);
            this.txt_addsurname.Name = "txt_addsurname";
            this.txt_addsurname.Size = new System.Drawing.Size(484, 22);
            this.txt_addsurname.TabIndex = 11;
            // 
            // label_phone
            // 
            this.label_phone.AutoSize = true;
            this.label_phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_phone.Location = new System.Drawing.Point(25, 211);
            this.label_phone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_phone.Name = "label_phone";
            this.label_phone.Size = new System.Drawing.Size(92, 25);
            this.label_phone.TabIndex = 12;
            this.label_phone.Text = "Telefon:";
            // 
            // txt_addphone
            // 
            this.txt_addphone.Location = new System.Drawing.Point(302, 211);
            this.txt_addphone.Margin = new System.Windows.Forms.Padding(4);
            this.txt_addphone.Name = "txt_addphone";
            this.txt_addphone.Size = new System.Drawing.Size(484, 22);
            this.txt_addphone.TabIndex = 13;
            // 
            // label_login
            // 
            this.label_login.AutoSize = true;
            this.label_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_login.Location = new System.Drawing.Point(25, 287);
            this.label_login.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_login.Name = "label_login";
            this.label_login.Size = new System.Drawing.Size(72, 25);
            this.label_login.TabIndex = 14;
            this.label_login.Text = "Login:";
            // 
            // label_passw
            // 
            this.label_passw.AutoSize = true;
            this.label_passw.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_passw.Location = new System.Drawing.Point(25, 335);
            this.label_passw.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_passw.Name = "label_passw";
            this.label_passw.Size = new System.Drawing.Size(74, 25);
            this.label_passw.TabIndex = 15;
            this.label_passw.Text = "Hasło:";
            // 
            // txt_addlogin
            // 
            this.txt_addlogin.Location = new System.Drawing.Point(302, 287);
            this.txt_addlogin.Margin = new System.Windows.Forms.Padding(4);
            this.txt_addlogin.Name = "txt_addlogin";
            this.txt_addlogin.Size = new System.Drawing.Size(484, 22);
            this.txt_addlogin.TabIndex = 16;
            this.txt_addlogin.TextChanged += new System.EventHandler(this.txt_addlogin_TextChanged);
            // 
            // txt_addpassw
            // 
            this.txt_addpassw.Location = new System.Drawing.Point(302, 335);
            this.txt_addpassw.Margin = new System.Windows.Forms.Padding(4);
            this.txt_addpassw.Name = "txt_addpassw";
            this.txt_addpassw.Size = new System.Drawing.Size(484, 22);
            this.txt_addpassw.TabIndex = 17;
            // 
            // btn_Close
            // 
            this.btn_Close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Close.Location = new System.Drawing.Point(918, 4);
            this.btn_Close.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(50, 40);
            this.btn_Close.TabIndex = 18;
            this.btn_Close.Text = "X";
            this.btn_Close.UseVisualStyleBackColor = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Bisque;
            this.panel2.Controls.Add(this.btn_Close);
            this.panel2.Controls.Add(this.txt_addpassw);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.txt_addlogin);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.label_passw);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.label_login);
            this.panel2.Controls.Add(this.label_name);
            this.panel2.Controls.Add(this.txt_addphone);
            this.panel2.Controls.Add(this.txt_addname);
            this.panel2.Controls.Add(this.label_phone);
            this.panel2.Controls.Add(this.label_surname);
            this.panel2.Controls.Add(this.txt_addsurname);
            this.panel2.Location = new System.Drawing.Point(36, 34);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(972, 439);
            this.panel2.TabIndex = 19;
            // 
            // edit_employees
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.CornflowerBlue;
            this.ClientSize = new System.Drawing.Size(1045, 506);
            this.Controls.Add(this.panel2);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "edit_employees";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "edit_employees";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.TextBox txt_addname;
        private System.Windows.Forms.Label label_surname;
        private System.Windows.Forms.TextBox txt_addsurname;
        private System.Windows.Forms.Label label_phone;
        private System.Windows.Forms.TextBox txt_addphone;
        private System.Windows.Forms.Label label_login;
        private System.Windows.Forms.Label label_passw;
        private System.Windows.Forms.TextBox txt_addlogin;
        private System.Windows.Forms.TextBox txt_addpassw;
        private System.Windows.Forms.Button btn_Close;
        private System.Windows.Forms.Panel panel2;
    }
}