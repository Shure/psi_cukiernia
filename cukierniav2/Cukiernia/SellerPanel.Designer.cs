﻿namespace Cukiernia
{
    partial class SellerPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SellerPanel));
            this.btn_Logout = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_Cakes = new System.Windows.Forms.Button();
            this.btn_Cookies = new System.Windows.Forms.Button();
            this.btn_Cake = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btn_Twirl = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_Order = new System.Windows.Forms.Button();
            this.btn_Invoice = new System.Windows.Forms.Button();
            this.btn_DailyReport = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView_listOfProducts = new System.Windows.Forms.DataGridView();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btn_EditProductReceipt = new System.Windows.Forms.Button();
            this.btn_CloseReceipt = new System.Windows.Forms.Button();
            this.btn_DeleteProductReceipt = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.txt_SumOfReceipt = new System.Windows.Forms.TextBox();
            this.listBox_Receipt = new System.Windows.Forms.ListBox();
            this.btn_addToList = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btn_AllOrders = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_listOfProducts)).BeginInit();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Logout
            // 
            this.btn_Logout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_Logout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Logout.ForeColor = System.Drawing.Color.Black;
            this.btn_Logout.Location = new System.Drawing.Point(1017, 0);
            this.btn_Logout.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Logout.Name = "btn_Logout";
            this.btn_Logout.Size = new System.Drawing.Size(50, 40);
            this.btn_Logout.TabIndex = 9;
            this.btn_Logout.Text = "X";
            this.btn_Logout.UseVisualStyleBackColor = false;
            this.btn_Logout.Click += new System.EventHandler(this.btn_Logout_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.YellowGreen;
            this.panel1.Controls.Add(this.btn_Cakes);
            this.panel1.Controls.Add(this.btn_Cookies);
            this.panel1.Controls.Add(this.btn_Cake);
            this.panel1.Controls.Add(this.pictureBox3);
            this.panel1.Controls.Add(this.btn_Twirl);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.pictureBox2);
            this.panel1.Controls.Add(this.pictureBox4);
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel1.Location = new System.Drawing.Point(13, 62);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(241, 479);
            this.panel1.TabIndex = 10;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // btn_Cakes
            // 
            this.btn_Cakes.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Cakes.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Cakes.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_Cakes.Location = new System.Drawing.Point(17, 202);
            this.btn_Cakes.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Cakes.Name = "btn_Cakes";
            this.btn_Cakes.Size = new System.Drawing.Size(204, 35);
            this.btn_Cakes.TabIndex = 8;
            this.btn_Cakes.Text = "Ciasta";
            this.btn_Cakes.UseVisualStyleBackColor = false;
            this.btn_Cakes.Click += new System.EventHandler(this.btn_Cakes_Click);
            // 
            // btn_Cookies
            // 
            this.btn_Cookies.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Cookies.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Cookies.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_Cookies.Location = new System.Drawing.Point(17, 432);
            this.btn_Cookies.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Cookies.Name = "btn_Cookies";
            this.btn_Cookies.Size = new System.Drawing.Size(204, 35);
            this.btn_Cookies.TabIndex = 7;
            this.btn_Cookies.Text = "Ciastka";
            this.btn_Cookies.UseVisualStyleBackColor = false;
            this.btn_Cookies.Click += new System.EventHandler(this.btn_Cookies_Click);
            // 
            // btn_Cake
            // 
            this.btn_Cake.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Cake.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Cake.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_Cake.Location = new System.Drawing.Point(17, 86);
            this.btn_Cake.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Cake.Name = "btn_Cake";
            this.btn_Cake.Size = new System.Drawing.Size(204, 35);
            this.btn_Cake.TabIndex = 5;
            this.btn_Cake.Text = "Torty";
            this.btn_Cake.UseVisualStyleBackColor = false;
            this.btn_Cake.Click += new System.EventHandler(this.btn_Cake_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(17, 129);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(204, 65);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            // 
            // btn_Twirl
            // 
            this.btn_Twirl.BackColor = System.Drawing.Color.DarkGreen;
            this.btn_Twirl.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Twirl.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_Twirl.Location = new System.Drawing.Point(17, 316);
            this.btn_Twirl.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Twirl.Name = "btn_Twirl";
            this.btn_Twirl.Size = new System.Drawing.Size(204, 35);
            this.btn_Twirl.TabIndex = 6;
            this.btn_Twirl.Text = "Rogale";
            this.btn_Twirl.UseVisualStyleBackColor = false;
            this.btn_Twirl.Click += new System.EventHandler(this.btn_Twirl_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(17, 15);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(204, 63);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(17, 245);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(204, 63);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(17, 359);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(204, 65);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 4;
            this.pictureBox4.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(100, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(316, 38);
            this.label1.TabIndex = 11;
            this.label1.Text = "Cukiernia \"Rogalik\"";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // btn_Order
            // 
            this.btn_Order.BackColor = System.Drawing.Color.Gold;
            this.btn_Order.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Order.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btn_Order.Location = new System.Drawing.Point(15, 86);
            this.btn_Order.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Order.Name = "btn_Order";
            this.btn_Order.Size = new System.Drawing.Size(204, 35);
            this.btn_Order.TabIndex = 12;
            this.btn_Order.Text = "Złóż zamówienie";
            this.btn_Order.UseVisualStyleBackColor = false;
            this.btn_Order.Click += new System.EventHandler(this.btn_Order_Click);
            // 
            // btn_Invoice
            // 
            this.btn_Invoice.BackColor = System.Drawing.Color.Gold;
            this.btn_Invoice.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Invoice.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btn_Invoice.Location = new System.Drawing.Point(17, 202);
            this.btn_Invoice.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Invoice.Name = "btn_Invoice";
            this.btn_Invoice.Size = new System.Drawing.Size(204, 35);
            this.btn_Invoice.TabIndex = 13;
            this.btn_Invoice.Text = "Wystaw fakturę";
            this.btn_Invoice.UseVisualStyleBackColor = false;
            this.btn_Invoice.Click += new System.EventHandler(this.button3_Click);
            // 
            // btn_DailyReport
            // 
            this.btn_DailyReport.BackColor = System.Drawing.Color.Gold;
            this.btn_DailyReport.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_DailyReport.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btn_DailyReport.Location = new System.Drawing.Point(15, 432);
            this.btn_DailyReport.Margin = new System.Windows.Forms.Padding(4);
            this.btn_DailyReport.Name = "btn_DailyReport";
            this.btn_DailyReport.Size = new System.Drawing.Size(204, 35);
            this.btn_DailyReport.TabIndex = 14;
            this.btn_DailyReport.Text = "Wygeneruj raport";
            this.btn_DailyReport.UseVisualStyleBackColor = false;
            this.btn_DailyReport.Click += new System.EventHandler(this.btn_DailyReport_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.YellowGreen;
            this.panel2.Controls.Add(this.dataGridView_listOfProducts);
            this.panel2.Location = new System.Drawing.Point(565, 62);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(485, 170);
            this.panel2.TabIndex = 18;
            // 
            // dataGridView_listOfProducts
            // 
            this.dataGridView_listOfProducts.AllowUserToAddRows = false;
            this.dataGridView_listOfProducts.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_listOfProducts.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_listOfProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_listOfProducts.Location = new System.Drawing.Point(7, 4);
            this.dataGridView_listOfProducts.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView_listOfProducts.Name = "dataGridView_listOfProducts";
            this.dataGridView_listOfProducts.RowHeadersVisible = false;
            this.dataGridView_listOfProducts.RowHeadersWidth = 51;
            this.dataGridView_listOfProducts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_listOfProducts.Size = new System.Drawing.Size(472, 159);
            this.dataGridView_listOfProducts.TabIndex = 0;
            this.dataGridView_listOfProducts.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.YellowGreen;
            this.panel3.Controls.Add(this.btn_EditProductReceipt);
            this.panel3.Controls.Add(this.btn_CloseReceipt);
            this.panel3.Controls.Add(this.btn_DeleteProductReceipt);
            this.panel3.Controls.Add(this.label7);
            this.panel3.Controls.Add(this.txt_SumOfReceipt);
            this.panel3.Controls.Add(this.listBox_Receipt);
            this.panel3.Controls.Add(this.btn_addToList);
            this.panel3.Controls.Add(this.label6);
            this.panel3.Controls.Add(this.label5);
            this.panel3.Controls.Add(this.label4);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.textBox4);
            this.panel3.Controls.Add(this.textBox3);
            this.panel3.Controls.Add(this.textBox2);
            this.panel3.Controls.Add(this.textBox1);
            this.panel3.Location = new System.Drawing.Point(565, 264);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(485, 277);
            this.panel3.TabIndex = 19;
            // 
            // btn_EditProductReceipt
            // 
            this.btn_EditProductReceipt.BackColor = System.Drawing.Color.LimeGreen;
            this.btn_EditProductReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_EditProductReceipt.Location = new System.Drawing.Point(260, 200);
            this.btn_EditProductReceipt.Margin = new System.Windows.Forms.Padding(4);
            this.btn_EditProductReceipt.Name = "btn_EditProductReceipt";
            this.btn_EditProductReceipt.Size = new System.Drawing.Size(100, 35);
            this.btn_EditProductReceipt.TabIndex = 12;
            this.btn_EditProductReceipt.Text = "Edytuj";
            this.btn_EditProductReceipt.UseVisualStyleBackColor = false;
            this.btn_EditProductReceipt.Click += new System.EventHandler(this.btn_EditProductReceipt_Click);
            // 
            // btn_CloseReceipt
            // 
            this.btn_CloseReceipt.BackColor = System.Drawing.Color.LimeGreen;
            this.btn_CloseReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_CloseReceipt.Location = new System.Drawing.Point(7, 200);
            this.btn_CloseReceipt.Margin = new System.Windows.Forms.Padding(4);
            this.btn_CloseReceipt.Name = "btn_CloseReceipt";
            this.btn_CloseReceipt.Size = new System.Drawing.Size(225, 35);
            this.btn_CloseReceipt.TabIndex = 20;
            this.btn_CloseReceipt.Text = "Przyjmij zamówienie";
            this.btn_CloseReceipt.UseVisualStyleBackColor = false;
            this.btn_CloseReceipt.Click += new System.EventHandler(this.btn_CloseReceipt_Click);
            // 
            // btn_DeleteProductReceipt
            // 
            this.btn_DeleteProductReceipt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_DeleteProductReceipt.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_DeleteProductReceipt.Location = new System.Drawing.Point(379, 200);
            this.btn_DeleteProductReceipt.Margin = new System.Windows.Forms.Padding(4);
            this.btn_DeleteProductReceipt.Name = "btn_DeleteProductReceipt";
            this.btn_DeleteProductReceipt.Size = new System.Drawing.Size(96, 35);
            this.btn_DeleteProductReceipt.TabIndex = 11;
            this.btn_DeleteProductReceipt.Text = "Usuń";
            this.btn_DeleteProductReceipt.UseVisualStyleBackColor = false;
            this.btn_DeleteProductReceipt.Click += new System.EventHandler(this.btn_DeleteProductReceipt_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(13, 243);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 20);
            this.label7.TabIndex = 10;
            this.label7.Text = "Suma [PLN]";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // txt_SumOfReceipt
            // 
            this.txt_SumOfReceipt.BackColor = System.Drawing.Color.White;
            this.txt_SumOfReceipt.ForeColor = System.Drawing.SystemColors.Window;
            this.txt_SumOfReceipt.Location = new System.Drawing.Point(129, 243);
            this.txt_SumOfReceipt.Margin = new System.Windows.Forms.Padding(4);
            this.txt_SumOfReceipt.Name = "txt_SumOfReceipt";
            this.txt_SumOfReceipt.Size = new System.Drawing.Size(346, 22);
            this.txt_SumOfReceipt.TabIndex = 9;
            // 
            // listBox_Receipt
            // 
            this.listBox_Receipt.FormattingEnabled = true;
            this.listBox_Receipt.ItemHeight = 16;
            this.listBox_Receipt.Location = new System.Drawing.Point(7, 108);
            this.listBox_Receipt.Margin = new System.Windows.Forms.Padding(4);
            this.listBox_Receipt.Name = "listBox_Receipt";
            this.listBox_Receipt.Size = new System.Drawing.Size(472, 84);
            this.listBox_Receipt.TabIndex = 9;
            // 
            // btn_addToList
            // 
            this.btn_addToList.BackColor = System.Drawing.Color.LimeGreen;
            this.btn_addToList.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_addToList.Location = new System.Drawing.Point(129, 53);
            this.btn_addToList.Margin = new System.Windows.Forms.Padding(4);
            this.btn_addToList.Name = "btn_addToList";
            this.btn_addToList.Size = new System.Drawing.Size(231, 35);
            this.btn_addToList.TabIndex = 8;
            this.btn_addToList.Text = "Dodaj do zamówienia";
            this.btn_addToList.UseVisualStyleBackColor = false;
            this.btn_addToList.Click += new System.EventHandler(this.btn_addToList_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(376, 2);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 17);
            this.label6.TabIndex = 7;
            this.label6.Text = "Koszt [PLN]";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.Location = new System.Drawing.Point(257, 2);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 17);
            this.label5.TabIndex = 6;
            this.label5.Text = "Rabat [PLN]";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(129, 2);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 17);
            this.label4.TabIndex = 5;
            this.label4.Text = "Liczba [szt.]";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(4, 2);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Wartość [PLN]";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // textBox4
            // 
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(379, 23);
            this.textBox4.Margin = new System.Windows.Forms.Padding(4);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(100, 22);
            this.textBox4.TabIndex = 3;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(260, 23);
            this.textBox3.Margin = new System.Windows.Forms.Padding(4);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(100, 22);
            this.textBox3.TabIndex = 2;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(132, 23);
            this.textBox2.Margin = new System.Windows.Forms.Padding(4);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 22);
            this.textBox2.TabIndex = 1;
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(7, 23);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(100, 22);
            this.textBox1.TabIndex = 0;
            // 
            // btn_AllOrders
            // 
            this.btn_AllOrders.BackColor = System.Drawing.Color.Gold;
            this.btn_AllOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_AllOrders.ForeColor = System.Drawing.SystemColors.Desktop;
            this.btn_AllOrders.Location = new System.Drawing.Point(15, 316);
            this.btn_AllOrders.Margin = new System.Windows.Forms.Padding(4);
            this.btn_AllOrders.Name = "btn_AllOrders";
            this.btn_AllOrders.Size = new System.Drawing.Size(204, 35);
            this.btn_AllOrders.TabIndex = 21;
            this.btn_AllOrders.Text = "Wszystkie zamówienia";
            this.btn_AllOrders.UseVisualStyleBackColor = false;
            this.btn_AllOrders.Click += new System.EventHandler(this.btn_AllOrders_Click);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.YellowGreen;
            this.panel4.Controls.Add(this.pictureBox8);
            this.panel4.Controls.Add(this.pictureBox7);
            this.panel4.Controls.Add(this.pictureBox6);
            this.panel4.Controls.Add(this.pictureBox5);
            this.panel4.Controls.Add(this.btn_Order);
            this.panel4.Controls.Add(this.btn_AllOrders);
            this.panel4.Controls.Add(this.btn_Invoice);
            this.panel4.Controls.Add(this.btn_DailyReport);
            this.panel4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel4.Location = new System.Drawing.Point(288, 62);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(241, 479);
            this.panel4.TabIndex = 22;
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox8.Image")));
            this.pictureBox8.Location = new System.Drawing.Point(60, 359);
            this.pictureBox8.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(122, 63);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 25;
            this.pictureBox8.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(60, 15);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(122, 63);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 24;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(60, 131);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(122, 63);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 23;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(60, 245);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(122, 63);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 22;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Click += new System.EventHandler(this.pictureBox5_Click);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.DarkGreen;
            this.panel5.Controls.Add(this.panel6);
            this.panel5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel5.Location = new System.Drawing.Point(13, 7);
            this.panel5.Margin = new System.Windows.Forms.Padding(4);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(516, 47);
            this.panel5.TabIndex = 23;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.DarkKhaki;
            this.panel6.Controls.Add(this.label1);
            this.panel6.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel6.Location = new System.Drawing.Point(4, 3);
            this.panel6.Margin = new System.Windows.Forms.Padding(4);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(508, 40);
            this.panel6.TabIndex = 24;
            // 
            // SellerPanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Green;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_Logout);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "SellerPanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SellerPanel";
            this.Load += new System.EventHandler(this.SellerPanel_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_listOfProducts)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_Logout;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_Order;
        private System.Windows.Forms.Button btn_Invoice;
        private System.Windows.Forms.Button btn_DailyReport;
        private System.Windows.Forms.Button btn_Cakes;
        private System.Windows.Forms.Button btn_Cookies;
        private System.Windows.Forms.Button btn_Twirl;
        private System.Windows.Forms.Button btn_Cake;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView_listOfProducts;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btn_addToList;
        private System.Windows.Forms.TextBox txt_SumOfReceipt;
        private System.Windows.Forms.ListBox listBox_Receipt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_CloseReceipt;
        private System.Windows.Forms.Button btn_AllOrders;
        private System.Windows.Forms.Button btn_EditProductReceipt;
        private System.Windows.Forms.Button btn_DeleteProductReceipt;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
    }
}
