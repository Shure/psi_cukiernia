﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cukiernia
{
    class Produkt
    {
        public int id;
        public int kategoria;
        public String nazwa;
        public float cena;
        public float rabat;
        public int ilosc;
        public int iloscWZamowieniu;

        public Produkt(int id, int kategoria, String nazwa, float cena, float rabat, int ilosc)
        {
            this.id = id;
            this.kategoria = kategoria;
            this.nazwa = nazwa;
            this.cena = cena;
            this.rabat = rabat;
            this.ilosc = ilosc;
        }

        public Produkt()
        {

        }

    }
}
