﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cukiernia
{
    class Zamowienie
    {
        private List<Produkt> produkty;
        public String data;
        public float rabat;
        public int id;
        public int statusId;

        public Zamowienie()
        {
            produkty = new List<Produkt>();
        }

        public void dodajProdukt(Produkt produkt)
        {
            produkty.Add(produkt);
        }

        public List<Produkt> listaProduktow()
        {
            return produkty;
        }

    }
}
