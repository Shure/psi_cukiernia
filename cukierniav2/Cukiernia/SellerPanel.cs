﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cukiernia
{
    public partial class SellerPanel : Form
    {

        int kategoria;
        Zamowienie paragon;
        float suma;
        float rabaty;
        string connectionString = "data source=localhost;initial catalog=Cukiernia;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework";

        public SellerPanel()
        {
            InitializeComponent();
            kategoria = 0;
            suma = 0;
            rabaty = 0;
            txt_SumOfReceipt.Text = "0";
            paragon = new Zamowienie();
        }

        private void lista_produktow()
        {
            string sql = "SELECT PR_id as id, PR_nazwa as Nazwa, PR_cena as 'Cena [zł]', PR_ilosc as 'Dostępna Ilość' FROM Produkt WHERE KA_id =" + kategoria;
            SqlConnection connection = new SqlConnection(connectionString);
            SqlDataAdapter dataadapter = new SqlDataAdapter(sql, connection);
            DataSet ds = new DataSet();
            connection.Open();
            dataadapter.Fill(ds, "Produkty");
            connection.Close();
            dataGridView_listOfProducts.DataSource = ds;
            dataGridView_listOfProducts.DataMember = "Produkty";
            dataGridView_listOfProducts.ClearSelection();
            clear();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Invoice invoice = new Invoice();
            invoice.Show();
        }

        private void btn_Logout_Click(object sender, EventArgs e)
        {
            this.Close();
            Shop_Form startPage = new Shop_Form();
            startPage.Show();

        }

        private void btn_Order_Click(object sender, EventArgs e)
        {
            Order order = new Order();
            order.Show();
        }

        private void btn_AllOrders_Click(object sender, EventArgs e)
        {
            AllOrders allOrders = new AllOrders();
            allOrders.Show();
        }

        private void btn_DailyReport_Click(object sender, EventArgs e)
        {
            Report report = new Report();
            report.Show();
        }

        private void btn_Cake_Click(object sender, EventArgs e)
        {
            kategoria = 1;
            lista_produktow();
        }

        private void SellerPanel_Load(object sender, EventArgs e)
        {

        }

        private void btn_Twirl_Click(object sender, EventArgs e)
        {
            kategoria = 2;
            lista_produktow();
        }

        private void btn_Cakes_Click(object sender, EventArgs e)
        {
            kategoria = 3;
            lista_produktow();
        }

        private void btn_Cookies_Click(object sender, EventArgs e)
        {
            kategoria = 4;
            lista_produktow();
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            textBox1.Text = dataGridView_listOfProducts.SelectedCells[2].Value.ToString();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            String value = textBox2.Text;
            try
            {
                int wybrane = int.Parse(value);
                int dostepne = int.Parse(dataGridView_listOfProducts.SelectedCells[3].Value.ToString());
                if (wybrane > dostepne)
                {
                    textBox2.Text = "";
                }
            }
            catch (Exception)
            {
                textBox2.Text = "";
            }
            zmienSume();

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            String value = textBox3.Text;
            try
            {
                float.Parse(value);
            }
            catch (Exception)
            {
                textBox3.Text = "";
            }
            zmienSume();
        }

        private void zmienSume()
        {
            float cena = 0;
            float ilosc = 0;
            float rabat = 0;
            if (!textBox2.Text.Equals(""))
            {
                cena = float.Parse(textBox1.Text);
            }
            if (!textBox2.Text.Equals(""))
            {
                ilosc = float.Parse(textBox2.Text);
            }
            if (!textBox3.Text.Equals(""))
            {
                rabat = float.Parse(textBox3.Text);
            }
                
            float suma = cena * ilosc - rabat;
            textBox4.Text = suma.ToString();
        }

        private void clear()
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
        }

        private void btn_addToList_Click(object sender, EventArgs e)
        {
            int id = int.Parse(dataGridView_listOfProducts.SelectedCells[0].Value.ToString());
            String nazwa = dataGridView_listOfProducts.SelectedCells[1].Value.ToString();
            float cena = float.Parse(dataGridView_listOfProducts.SelectedCells[2].Value.ToString());
            float rabat = 0;
            if (!textBox3.Text.Equals(""))
            {
                rabat = float.Parse(textBox3.Text);
            }
            int ilosc = 1;
            if (!textBox2.Text.Equals(""))
            {
                ilosc = int.Parse(textBox2.Text);
            }
            Produkt nowyProdukt = new Produkt(id, kategoria, nazwa, cena, rabat, ilosc);
            paragon.dodajProdukt(nowyProdukt);

            listBox_Receipt.Items.Add(nazwa + " x" + ilosc + " = " + (cena * ilosc - rabat) + "zł ( rabat: " + rabat + "zł)");
            suma = suma + (cena * ilosc - rabat);
            rabaty = rabaty + rabat;
            txt_SumOfReceipt.Text = suma.ToString();
        }

        private void btn_CloseReceipt_Click(object sender, EventArgs e)
        {
            List<Produkt> produkty = paragon.listaProduktow();
            DateTime myDateTime = DateTime.Now;
            string sqlFormattedDate = myDateTime.ToString("yyyy-MM-dd HH:mm:ss.fff");

            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand command = connection.CreateCommand();
            SqlTransaction transaction = connection.BeginTransaction();
            command.Transaction = transaction;
            command.Parameters.Add("@ID", SqlDbType.Int, 4).Direction = ParameterDirection.Output;
            try
            {
                command.CommandText = "Insert into Zamowienie (ZA_data, ZA_rabat, P_id, ST_id) VALUES ( '" + sqlFormattedDate + "', " + rabaty.ToString() + ", 1, 1); SELECT @@IDENTITY";
                
                int zamowienieId = Convert.ToInt32(command.ExecuteScalar());
                foreach (Produkt produkt in produkty)
                {
                    command.CommandText = "Insert into Produkty_zamowienia (ZA_id, PR_id, ilosc_produktow) values (" + zamowienieId + ", " + produkt.id + ", " + produkt.ilosc + ")";
                    command.ExecuteNonQuery();
                }
                transaction.Commit();
            } catch(Exception ex)
            {
                transaction.Rollback();
            }
            connection.Close();


            paragon = new Zamowienie();
            suma = 0;
            rabaty = 0;
            clear();
            txt_SumOfReceipt.Text = "";
            listBox_Receipt.Items.Clear();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_EditProductReceipt_Click(object sender, EventArgs e)
        {

        }

        private void btn_DeleteProductReceipt_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {

        }
    }
}
