﻿namespace Cukiernia
{
    partial class Report
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_DailyReport = new System.Windows.Forms.Button();
            this.btn_WeeklyReport = new System.Windows.Forms.Button();
            this.btn_MonthlyReport = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_Logout = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // btn_DailyReport
            // 
            this.btn_DailyReport.BackColor = System.Drawing.Color.Silver;
            this.btn_DailyReport.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_DailyReport.Location = new System.Drawing.Point(16, 15);
            this.btn_DailyReport.Margin = new System.Windows.Forms.Padding(4);
            this.btn_DailyReport.Name = "btn_DailyReport";
            this.btn_DailyReport.Size = new System.Drawing.Size(119, 46);
            this.btn_DailyReport.TabIndex = 15;
            this.btn_DailyReport.Text = "Dzienny";
            this.btn_DailyReport.UseVisualStyleBackColor = false;
            // 
            // btn_WeeklyReport
            // 
            this.btn_WeeklyReport.BackColor = System.Drawing.Color.Silver;
            this.btn_WeeklyReport.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_WeeklyReport.Location = new System.Drawing.Point(143, 15);
            this.btn_WeeklyReport.Margin = new System.Windows.Forms.Padding(4);
            this.btn_WeeklyReport.Name = "btn_WeeklyReport";
            this.btn_WeeklyReport.Size = new System.Drawing.Size(167, 46);
            this.btn_WeeklyReport.TabIndex = 16;
            this.btn_WeeklyReport.Text = "Tygodniowy";
            this.btn_WeeklyReport.UseVisualStyleBackColor = false;
            // 
            // btn_MonthlyReport
            // 
            this.btn_MonthlyReport.BackColor = System.Drawing.Color.Silver;
            this.btn_MonthlyReport.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_MonthlyReport.Location = new System.Drawing.Point(317, 15);
            this.btn_MonthlyReport.Margin = new System.Windows.Forms.Padding(4);
            this.btn_MonthlyReport.Name = "btn_MonthlyReport";
            this.btn_MonthlyReport.Size = new System.Drawing.Size(144, 46);
            this.btn_MonthlyReport.TabIndex = 17;
            this.btn_MonthlyReport.Text = "Miesięczny";
            this.btn_MonthlyReport.UseVisualStyleBackColor = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Wheat;
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Location = new System.Drawing.Point(16, 80);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1035, 384);
            this.panel1.TabIndex = 18;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(21, 15);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.Size = new System.Drawing.Size(995, 354);
            this.dataGridView1.TabIndex = 0;
            // 
            // btn_Logout
            // 
            this.btn_Logout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_Logout.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Logout.ForeColor = System.Drawing.Color.Black;
            this.btn_Logout.Location = new System.Drawing.Point(1014, 2);
            this.btn_Logout.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Logout.Name = "btn_Logout";
            this.btn_Logout.Size = new System.Drawing.Size(50, 45);
            this.btn_Logout.TabIndex = 19;
            this.btn_Logout.Text = "X";
            this.btn_Logout.UseVisualStyleBackColor = false;
            this.btn_Logout.Click += new System.EventHandler(this.btn_Logout_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(734, 494);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(317, 22);
            this.textBox1.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.label1.Location = new System.Drawing.Point(598, 494);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 22);
            this.label1.TabIndex = 21;
            this.label1.Text = "Kwota [PLN]";
            // 
            // Report
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gold;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btn_Logout);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_MonthlyReport);
            this.Controls.Add(this.btn_WeeklyReport);
            this.Controls.Add(this.btn_DailyReport);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Report";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Report";
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_DailyReport;
        private System.Windows.Forms.Button btn_WeeklyReport;
        private System.Windows.Forms.Button btn_MonthlyReport;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_Logout;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView1;
    }
}