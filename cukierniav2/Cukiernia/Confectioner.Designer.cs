﻿namespace Cukiernia
{
    partial class Confectioner
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_Logout = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.dataGridView_InProgressOrders = new System.Windows.Forms.DataGridView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_DataLater = new System.Windows.Forms.Button();
            this.btn_DataEalier = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.listView_ReadyOrders = new System.Windows.Forms.ListView();
            this.btn_SendReadyOrders = new System.Windows.Forms.Button();
            this.btn_AddToListReadyOrders = new System.Windows.Forms.Button();
            this.btn_Request = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btn_ShowAllOrders = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_InProgressOrders)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_Logout
            // 
            this.btn_Logout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_Logout.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Logout.ForeColor = System.Drawing.Color.Black;
            this.btn_Logout.Location = new System.Drawing.Point(1016, 2);
            this.btn_Logout.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Logout.Name = "btn_Logout";
            this.btn_Logout.Size = new System.Drawing.Size(50, 45);
            this.btn_Logout.TabIndex = 10;
            this.btn_Logout.Text = "X";
            this.btn_Logout.UseVisualStyleBackColor = false;
            this.btn_Logout.Click += new System.EventHandler(this.btn_Logout_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.MistyRose;
            this.panel1.Controls.Add(this.dataGridView_InProgressOrders);
            this.panel1.Location = new System.Drawing.Point(31, 57);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(839, 196);
            this.panel1.TabIndex = 14;
            // 
            // dataGridView_InProgressOrders
            // 
            this.dataGridView_InProgressOrders.AllowUserToAddRows = false;
            this.dataGridView_InProgressOrders.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_InProgressOrders.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView_InProgressOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_InProgressOrders.Location = new System.Drawing.Point(4, 4);
            this.dataGridView_InProgressOrders.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView_InProgressOrders.Name = "dataGridView_InProgressOrders";
            this.dataGridView_InProgressOrders.RowHeadersVisible = false;
            this.dataGridView_InProgressOrders.RowHeadersWidth = 51;
            this.dataGridView_InProgressOrders.RowTemplate.DefaultCellStyle.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_InProgressOrders.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView_InProgressOrders.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridView_InProgressOrders.Size = new System.Drawing.Size(831, 188);
            this.dataGridView_InProgressOrders.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Bisque;
            this.panel2.Controls.Add(this.btn_DataLater);
            this.panel2.Controls.Add(this.btn_DataEalier);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Location = new System.Drawing.Point(877, 57);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(180, 156);
            this.panel2.TabIndex = 15;
            // 
            // btn_DataLater
            // 
            this.btn_DataLater.BackColor = System.Drawing.Color.Silver;
            this.btn_DataLater.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_DataLater.ForeColor = System.Drawing.Color.Black;
            this.btn_DataLater.Location = new System.Drawing.Point(4, 96);
            this.btn_DataLater.Margin = new System.Windows.Forms.Padding(4);
            this.btn_DataLater.Name = "btn_DataLater";
            this.btn_DataLater.Size = new System.Drawing.Size(170, 50);
            this.btn_DataLater.TabIndex = 23;
            this.btn_DataLater.Text = "Od najpóźniejszej";
            this.btn_DataLater.UseVisualStyleBackColor = false;
            // 
            // btn_DataEalier
            // 
            this.btn_DataEalier.BackColor = System.Drawing.Color.Silver;
            this.btn_DataEalier.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_DataEalier.ForeColor = System.Drawing.Color.Black;
            this.btn_DataEalier.Location = new System.Drawing.Point(4, 38);
            this.btn_DataEalier.Margin = new System.Windows.Forms.Padding(4);
            this.btn_DataEalier.Name = "btn_DataEalier";
            this.btn_DataEalier.Size = new System.Drawing.Size(170, 50);
            this.btn_DataEalier.TabIndex = 22;
            this.btn_DataEalier.Text = "Od najwcześniejszej";
            this.btn_DataEalier.UseMnemonic = false;
            this.btn_DataEalier.UseVisualStyleBackColor = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.ForeColor = System.Drawing.Color.DarkRed;
            this.label3.Location = new System.Drawing.Point(6, 10);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(163, 20);
            this.label3.TabIndex = 21;
            this.label3.Text = "Filtruj według daty";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(31, 33);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(278, 20);
            this.label1.TabIndex = 16;
            this.label1.Text = "Zamówienia w trakcie realizacji:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(31, 279);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(229, 20);
            this.label2.TabIndex = 17;
            this.label2.Text = "Zamówienia zrealizowane:";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.MistyRose;
            this.panel3.Controls.Add(this.listView_ReadyOrders);
            this.panel3.Location = new System.Drawing.Point(31, 305);
            this.panel3.Margin = new System.Windows.Forms.Padding(4);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(839, 210);
            this.panel3.TabIndex = 15;
            // 
            // listView_ReadyOrders
            // 
            this.listView_ReadyOrders.HideSelection = false;
            this.listView_ReadyOrders.Location = new System.Drawing.Point(4, 10);
            this.listView_ReadyOrders.Margin = new System.Windows.Forms.Padding(4);
            this.listView_ReadyOrders.Name = "listView_ReadyOrders";
            this.listView_ReadyOrders.Size = new System.Drawing.Size(829, 189);
            this.listView_ReadyOrders.TabIndex = 0;
            this.listView_ReadyOrders.UseCompatibleStateImageBehavior = false;
            // 
            // btn_SendReadyOrders
            // 
            this.btn_SendReadyOrders.BackColor = System.Drawing.Color.Silver;
            this.btn_SendReadyOrders.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btn_SendReadyOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_SendReadyOrders.ForeColor = System.Drawing.Color.Black;
            this.btn_SendReadyOrders.Location = new System.Drawing.Point(4, 126);
            this.btn_SendReadyOrders.Margin = new System.Windows.Forms.Padding(4);
            this.btn_SendReadyOrders.Name = "btn_SendReadyOrders";
            this.btn_SendReadyOrders.Size = new System.Drawing.Size(170, 55);
            this.btn_SendReadyOrders.TabIndex = 23;
            this.btn_SendReadyOrders.Text = "Wyślij listę zrealizowanych zamówień";
            this.btn_SendReadyOrders.UseVisualStyleBackColor = false;
            // 
            // btn_AddToListReadyOrders
            // 
            this.btn_AddToListReadyOrders.BackColor = System.Drawing.Color.Silver;
            this.btn_AddToListReadyOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_AddToListReadyOrders.ForeColor = System.Drawing.Color.Black;
            this.btn_AddToListReadyOrders.Location = new System.Drawing.Point(4, 68);
            this.btn_AddToListReadyOrders.Margin = new System.Windows.Forms.Padding(4);
            this.btn_AddToListReadyOrders.Name = "btn_AddToListReadyOrders";
            this.btn_AddToListReadyOrders.Size = new System.Drawing.Size(170, 50);
            this.btn_AddToListReadyOrders.TabIndex = 22;
            this.btn_AddToListReadyOrders.Text = "Dodaj do listy zrealizowanych";
            this.btn_AddToListReadyOrders.UseVisualStyleBackColor = false;
            // 
            // btn_Request
            // 
            this.btn_Request.BackColor = System.Drawing.Color.Silver;
            this.btn_Request.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Request.ForeColor = System.Drawing.Color.Black;
            this.btn_Request.Location = new System.Drawing.Point(4, 10);
            this.btn_Request.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Request.Name = "btn_Request";
            this.btn_Request.Size = new System.Drawing.Size(170, 50);
            this.btn_Request.TabIndex = 21;
            this.btn_Request.Text = "Zgłoś zapotrzebowanie";
            this.btn_Request.UseVisualStyleBackColor = false;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Bisque;
            this.panel4.Controls.Add(this.btn_ShowAllOrders);
            this.panel4.Controls.Add(this.btn_Request);
            this.panel4.Controls.Add(this.btn_AddToListReadyOrders);
            this.panel4.Controls.Add(this.btn_SendReadyOrders);
            this.panel4.Location = new System.Drawing.Point(877, 279);
            this.panel4.Margin = new System.Windows.Forms.Padding(4);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(180, 255);
            this.panel4.TabIndex = 25;
            // 
            // btn_ShowAllOrders
            // 
            this.btn_ShowAllOrders.BackColor = System.Drawing.Color.Silver;
            this.btn_ShowAllOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_ShowAllOrders.ForeColor = System.Drawing.Color.Black;
            this.btn_ShowAllOrders.Location = new System.Drawing.Point(4, 189);
            this.btn_ShowAllOrders.Margin = new System.Windows.Forms.Padding(4);
            this.btn_ShowAllOrders.Name = "btn_ShowAllOrders";
            this.btn_ShowAllOrders.Size = new System.Drawing.Size(170, 55);
            this.btn_ShowAllOrders.TabIndex = 26;
            this.btn_ShowAllOrders.Text = "Wszystkie zamówienia";
            this.btn_ShowAllOrders.UseVisualStyleBackColor = false;
            // 
            // Confectioner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSalmon;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btn_Logout);
            this.ForeColor = System.Drawing.Color.LightGreen;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Confectioner";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Confecttioner";
            this.Load += new System.EventHandler(this.Confectioner_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_InProgressOrders)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_Logout;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView dataGridView_InProgressOrders;
        private System.Windows.Forms.ListView listView_ReadyOrders;
        private System.Windows.Forms.Button btn_DataLater;
        private System.Windows.Forms.Button btn_DataEalier;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_SendReadyOrders;
        private System.Windows.Forms.Button btn_AddToListReadyOrders;
        private System.Windows.Forms.Button btn_Request;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btn_ShowAllOrders;
    }
}