﻿namespace Cukiernia
{
    partial class add_employees
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_save = new System.Windows.Forms.Button();
            this.btn_cancel = new System.Windows.Forms.Button();
            this.txt_addname = new System.Windows.Forms.TextBox();
            this.txt_addsurname = new System.Windows.Forms.TextBox();
            this.txt_addphone = new System.Windows.Forms.TextBox();
            this.txt_addlogin = new System.Windows.Forms.TextBox();
            this.txt_addpassw = new System.Windows.Forms.TextBox();
            this.label_name = new System.Windows.Forms.Label();
            this.label_surname = new System.Windows.Forms.Label();
            this.label_phone = new System.Windows.Forms.Label();
            this.label_login = new System.Windows.Forms.Label();
            this.label_passw = new System.Windows.Forms.Label();
            this.label_add_employees = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btn_Close = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_save
            // 
            this.btn_save.BackColor = System.Drawing.Color.Silver;
            this.btn_save.Location = new System.Drawing.Point(263, 347);
            this.btn_save.Margin = new System.Windows.Forms.Padding(4);
            this.btn_save.Name = "btn_save";
            this.btn_save.Size = new System.Drawing.Size(133, 43);
            this.btn_save.TabIndex = 0;
            this.btn_save.Text = "Zatwierdź";
            this.btn_save.UseVisualStyleBackColor = false;
            // 
            // btn_cancel
            // 
            this.btn_cancel.BackColor = System.Drawing.Color.Silver;
            this.btn_cancel.Location = new System.Drawing.Point(614, 347);
            this.btn_cancel.Margin = new System.Windows.Forms.Padding(4);
            this.btn_cancel.Name = "btn_cancel";
            this.btn_cancel.Size = new System.Drawing.Size(133, 43);
            this.btn_cancel.TabIndex = 1;
            this.btn_cancel.Text = "Anuluj";
            this.btn_cancel.UseVisualStyleBackColor = false;
            // 
            // txt_addname
            // 
            this.txt_addname.Location = new System.Drawing.Point(263, 113);
            this.txt_addname.Margin = new System.Windows.Forms.Padding(4);
            this.txt_addname.Name = "txt_addname";
            this.txt_addname.Size = new System.Drawing.Size(484, 22);
            this.txt_addname.TabIndex = 2;
            this.txt_addname.TextChanged += new System.EventHandler(this.txt_addname_TextChanged);
            // 
            // txt_addsurname
            // 
            this.txt_addsurname.Location = new System.Drawing.Point(263, 153);
            this.txt_addsurname.Margin = new System.Windows.Forms.Padding(4);
            this.txt_addsurname.Name = "txt_addsurname";
            this.txt_addsurname.Size = new System.Drawing.Size(484, 22);
            this.txt_addsurname.TabIndex = 3;
            // 
            // txt_addphone
            // 
            this.txt_addphone.Location = new System.Drawing.Point(263, 191);
            this.txt_addphone.Margin = new System.Windows.Forms.Padding(4);
            this.txt_addphone.Name = "txt_addphone";
            this.txt_addphone.Size = new System.Drawing.Size(484, 22);
            this.txt_addphone.TabIndex = 4;
            // 
            // txt_addlogin
            // 
            this.txt_addlogin.Location = new System.Drawing.Point(263, 248);
            this.txt_addlogin.Margin = new System.Windows.Forms.Padding(4);
            this.txt_addlogin.Name = "txt_addlogin";
            this.txt_addlogin.Size = new System.Drawing.Size(484, 22);
            this.txt_addlogin.TabIndex = 5;
            // 
            // txt_addpassw
            // 
            this.txt_addpassw.Location = new System.Drawing.Point(263, 285);
            this.txt_addpassw.Margin = new System.Windows.Forms.Padding(4);
            this.txt_addpassw.Name = "txt_addpassw";
            this.txt_addpassw.Size = new System.Drawing.Size(484, 22);
            this.txt_addpassw.TabIndex = 6;
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_name.Location = new System.Drawing.Point(34, 109);
            this.label_name.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(59, 25);
            this.label_name.TabIndex = 7;
            this.label_name.Text = "Imię:";
            // 
            // label_surname
            // 
            this.label_surname.AutoSize = true;
            this.label_surname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_surname.Location = new System.Drawing.Point(34, 149);
            this.label_surname.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_surname.Name = "label_surname";
            this.label_surname.Size = new System.Drawing.Size(111, 25);
            this.label_surname.TabIndex = 8;
            this.label_surname.Text = "Nazwisko:";
            // 
            // label_phone
            // 
            this.label_phone.AutoSize = true;
            this.label_phone.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_phone.Location = new System.Drawing.Point(34, 191);
            this.label_phone.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_phone.Name = "label_phone";
            this.label_phone.Size = new System.Drawing.Size(92, 25);
            this.label_phone.TabIndex = 9;
            this.label_phone.Text = "Telefon:";
            // 
            // label_login
            // 
            this.label_login.AutoSize = true;
            this.label_login.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_login.Location = new System.Drawing.Point(34, 248);
            this.label_login.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_login.Name = "label_login";
            this.label_login.Size = new System.Drawing.Size(72, 25);
            this.label_login.TabIndex = 10;
            this.label_login.Text = "Login:";
            // 
            // label_passw
            // 
            this.label_passw.AutoSize = true;
            this.label_passw.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_passw.Location = new System.Drawing.Point(34, 285);
            this.label_passw.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_passw.Name = "label_passw";
            this.label_passw.Size = new System.Drawing.Size(74, 25);
            this.label_passw.TabIndex = 11;
            this.label_passw.Text = "Hasło:";
            // 
            // label_add_employees
            // 
            this.label_add_employees.AutoSize = true;
            this.label_add_employees.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_add_employees.Location = new System.Drawing.Point(367, 30);
            this.label_add_employees.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label_add_employees.Name = "label_add_employees";
            this.label_add_employees.Size = new System.Drawing.Size(280, 39);
            this.label_add_employees.TabIndex = 12;
            this.label_add_employees.Text = "Nowy pracownik";
            this.label_add_employees.Click += new System.EventHandler(this.label1_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Bisque;
            this.panel2.Controls.Add(this.btn_Close);
            this.panel2.Controls.Add(this.label_name);
            this.panel2.Controls.Add(this.label_add_employees);
            this.panel2.Controls.Add(this.label_surname);
            this.panel2.Controls.Add(this.btn_cancel);
            this.panel2.Controls.Add(this.txt_addpassw);
            this.panel2.Controls.Add(this.btn_save);
            this.panel2.Controls.Add(this.label_passw);
            this.panel2.Controls.Add(this.txt_addlogin);
            this.panel2.Controls.Add(this.label_phone);
            this.panel2.Controls.Add(this.txt_addphone);
            this.panel2.Controls.Add(this.label_login);
            this.panel2.Controls.Add(this.txt_addsurname);
            this.panel2.Controls.Add(this.txt_addname);
            this.panel2.Location = new System.Drawing.Point(31, 33);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(968, 439);
            this.panel2.TabIndex = 16;
            // 
            // btn_Close
            // 
            this.btn_Close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Close.Location = new System.Drawing.Point(914, 4);
            this.btn_Close.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(50, 40);
            this.btn_Close.TabIndex = 17;
            this.btn_Close.Text = "X";
            this.btn_Close.UseVisualStyleBackColor = false;
            // 
            // add_employees
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.CornflowerBlue;
            this.ClientSize = new System.Drawing.Size(1045, 506);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "add_employees";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "add_employees";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_save;
        private System.Windows.Forms.Button btn_cancel;
        private System.Windows.Forms.TextBox txt_addname;
        private System.Windows.Forms.TextBox txt_addsurname;
        private System.Windows.Forms.TextBox txt_addphone;
        private System.Windows.Forms.TextBox txt_addlogin;
        private System.Windows.Forms.TextBox txt_addpassw;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.Label label_surname;
        private System.Windows.Forms.Label label_phone;
        private System.Windows.Forms.Label label_login;
        private System.Windows.Forms.Label label_passw;
        private System.Windows.Forms.Label label_add_employees;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btn_Close;
    }
}