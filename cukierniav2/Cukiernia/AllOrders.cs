﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cukiernia
{
    public partial class AllOrders : Form
    {

        string connectionString = "data source=localhost;initial catalog=Cukiernia;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework";


        public AllOrders()
        {
            InitializeComponent();

            List<Zamowienie> zamowienia = wczytajListeZamowien();

            dataGridView_AllOrders.ColumnCount = 6;
            dataGridView_AllOrders.Columns[0].Name = "ID";
            dataGridView_AllOrders.Columns[1].Name = "Data";
            dataGridView_AllOrders.Columns[2].Name = "Status";
            dataGridView_AllOrders.Columns[3].Name = "Produkty";
            dataGridView_AllOrders.Columns[4].Name = "Suma";
            dataGridView_AllOrders.Columns[5].Name = "Rabat";

            foreach (Zamowienie zam in zamowienia)
            {
                String produkty = "";
                float suma = 0;
                foreach (Produkt produkt in zam.listaProduktow())
                {
                    produkty += produkt.nazwa + " x" + produkt.iloscWZamowieniu + Environment.NewLine;
                    suma += produkt.cena * produkt.iloscWZamowieniu;
                }
                suma = suma - zam.rabat;
                String status = "-";
                if(zam.statusId == 1)
                {
                    status = "W trakcie";
                } else if (zam.statusId == 2)
                {
                    status = "Gotowe";
                }
                string[] row = new string[] { zam.id.ToString(), zam.data.ToString(), status, produkty, suma.ToString(), zam.rabat.ToString()};
                dataGridView_AllOrders.Rows.Add(row);
            }
        }

        private List<Zamowienie> wczytajListeZamowien()
        {
            List<Zamowienie> zamowienia = new List<Zamowienie>();
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            SqlCommand command = connection.CreateCommand();
            command.CommandText = "SELECT * FROM Zamowienie";
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                while (reader.Read())
                {
                    Zamowienie zamowienie = new Zamowienie();
                    zamowienie.id = (int)reader["ZA_id"];
                    zamowienie.data = reader["ZA_data"].ToString();
                    zamowienie.statusId = (int)reader["ST_id"];
                    zamowienie.rabat = float.Parse(reader["ZA_rabat"].ToString());
                    zamowienia.Add(zamowienie);
                }
            }
            finally
            {
                reader.Close();
            }


            foreach (Zamowienie zamowienie in zamowienia)
            {
                List<int> ids = new List<int>();
                command.CommandText = "SELECT PR_id FROM Produkty_zamowienia WHERE ZA_id = " + zamowienie.id;
                reader = command.ExecuteReader();
                while (reader.Read())
                {
                    ids.Add((int)reader["PR_id"]);
                }
                reader.Close();

                foreach (int id in ids)
                {
                    Produkt produkt = new Produkt();
                    command.CommandText = "SELECT * FROM Produkt WHERE PR_id = " + id;
                    reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        produkt.kategoria = (int)reader["KA_id"];
                        produkt.nazwa = reader["PR_nazwa"].ToString();
                        produkt.rabat = 0;
                        produkt.ilosc = (int)reader["PR_ilosc"];
                        produkt.cena = float.Parse(reader["PR_cena"].ToString());
                    }
                    reader.Close();

                    command.CommandText = "SELECT ilosc_produktow FROM Produkty_zamowienia WHERE PR_id = " + id + " AND ZA_id = " + zamowienie.id;
                    reader = command.ExecuteReader();
                    while (reader.Read())
                    {

                        produkt.iloscWZamowieniu = (int)reader["ilosc_produktow"];
                    }
                    reader.Close();

                    zamowienie.dodajProdukt(produkt);
                }
            }

            return zamowienia;
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btn_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
