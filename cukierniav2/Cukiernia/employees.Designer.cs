﻿namespace Cukiernia
{
    partial class employees
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_add_employees = new System.Windows.Forms.Button();
            this.btn_delete_employees = new System.Windows.Forms.Button();
            this.btn_edit_employees = new System.Windows.Forms.Button();
            this.btn_Close = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ButtonShadow;
            this.panel1.Location = new System.Drawing.Point(29, 15);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(677, 448);
            this.panel1.TabIndex = 0;
            // 
            // btn_add_employees
            // 
            this.btn_add_employees.BackColor = System.Drawing.Color.Silver;
            this.btn_add_employees.Location = new System.Drawing.Point(761, 93);
            this.btn_add_employees.Margin = new System.Windows.Forms.Padding(4);
            this.btn_add_employees.Name = "btn_add_employees";
            this.btn_add_employees.Size = new System.Drawing.Size(227, 49);
            this.btn_add_employees.TabIndex = 1;
            this.btn_add_employees.Text = "Dodaj pracownika";
            this.btn_add_employees.UseVisualStyleBackColor = false;
            // 
            // btn_delete_employees
            // 
            this.btn_delete_employees.BackColor = System.Drawing.Color.Silver;
            this.btn_delete_employees.Location = new System.Drawing.Point(761, 207);
            this.btn_delete_employees.Margin = new System.Windows.Forms.Padding(4);
            this.btn_delete_employees.Name = "btn_delete_employees";
            this.btn_delete_employees.Size = new System.Drawing.Size(227, 49);
            this.btn_delete_employees.TabIndex = 2;
            this.btn_delete_employees.Text = "Usuń pozycję";
            this.btn_delete_employees.UseVisualStyleBackColor = false;
            // 
            // btn_edit_employees
            // 
            this.btn_edit_employees.BackColor = System.Drawing.Color.Silver;
            this.btn_edit_employees.Location = new System.Drawing.Point(761, 321);
            this.btn_edit_employees.Margin = new System.Windows.Forms.Padding(4);
            this.btn_edit_employees.Name = "btn_edit_employees";
            this.btn_edit_employees.Size = new System.Drawing.Size(227, 49);
            this.btn_edit_employees.TabIndex = 3;
            this.btn_edit_employees.Text = "Edytuj pozycję";
            this.btn_edit_employees.UseVisualStyleBackColor = false;
            // 
            // btn_Close
            // 
            this.btn_Close.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Close.Location = new System.Drawing.Point(987, 3);
            this.btn_Close.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Close.Name = "btn_Close";
            this.btn_Close.Size = new System.Drawing.Size(50, 40);
            this.btn_Close.TabIndex = 11;
            this.btn_Close.Text = "X";
            this.btn_Close.UseVisualStyleBackColor = false;
            // 
            // employees
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.CornflowerBlue;
            this.ClientSize = new System.Drawing.Size(1040, 492);
            this.Controls.Add(this.btn_Close);
            this.Controls.Add(this.btn_edit_employees);
            this.Controls.Add(this.btn_delete_employees);
            this.Controls.Add(this.btn_add_employees);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "employees";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "employees";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_add_employees;
        private System.Windows.Forms.Button btn_delete_employees;
        private System.Windows.Forms.Button btn_edit_employees;
        private System.Windows.Forms.Button btn_Close;
    }
}