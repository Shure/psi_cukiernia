﻿namespace Cukiernia
{
    partial class Warehouseman
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txt_NameAssortment = new System.Windows.Forms.TextBox();
            this.btn_AcceptAssortment = new System.Windows.Forms.Button();
            this.txt_NrAssortment = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btn_Logout = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btn_AcceptQuantity = new System.Windows.Forms.Button();
            this.txt_NameAssortmentStock = new System.Windows.Forms.TextBox();
            this.txt_AssortmentActuallyQuantity = new System.Windows.Forms.TextBox();
            this.txt_AssortmentSystemQuantity = new System.Windows.Forms.TextBox();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Plum;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txt_NameAssortment);
            this.panel1.Controls.Add(this.btn_AcceptAssortment);
            this.panel1.Controls.Add(this.txt_NrAssortment);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.dataGridView1);
            this.panel1.Controls.Add(this.btn_Logout);
            this.panel1.Location = new System.Drawing.Point(16, 15);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1035, 304);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label3.Location = new System.Drawing.Point(773, 138);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 18);
            this.label3.TabIndex = 16;
            this.label3.Text = "Liczba:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label2.Location = new System.Drawing.Point(772, 77);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 18);
            this.label2.TabIndex = 15;
            this.label2.Text = "Nazwa:";
            // 
            // txt_NameAssortment
            // 
            this.txt_NameAssortment.Location = new System.Drawing.Point(771, 99);
            this.txt_NameAssortment.Margin = new System.Windows.Forms.Padding(4);
            this.txt_NameAssortment.Name = "txt_NameAssortment";
            this.txt_NameAssortment.ReadOnly = true;
            this.txt_NameAssortment.Size = new System.Drawing.Size(253, 22);
            this.txt_NameAssortment.TabIndex = 14;
            // 
            // btn_AcceptAssortment
            // 
            this.btn_AcceptAssortment.BackColor = System.Drawing.Color.Silver;
            this.btn_AcceptAssortment.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_AcceptAssortment.Location = new System.Drawing.Point(814, 254);
            this.btn_AcceptAssortment.Margin = new System.Windows.Forms.Padding(4);
            this.btn_AcceptAssortment.Name = "btn_AcceptAssortment";
            this.btn_AcceptAssortment.Size = new System.Drawing.Size(170, 30);
            this.btn_AcceptAssortment.TabIndex = 13;
            this.btn_AcceptAssortment.Text = "Zatwierdź";
            this.btn_AcceptAssortment.UseVisualStyleBackColor = false;
            // 
            // txt_NrAssortment
            // 
            this.txt_NrAssortment.Location = new System.Drawing.Point(771, 160);
            this.txt_NrAssortment.Margin = new System.Windows.Forms.Padding(4);
            this.txt_NrAssortment.Name = "txt_NrAssortment";
            this.txt_NrAssortment.Size = new System.Drawing.Size(253, 22);
            this.txt_NrAssortment.TabIndex = 12;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label1.Location = new System.Drawing.Point(20, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 22);
            this.label1.TabIndex = 1;
            this.label1.Text = "Asortyment:";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(24, 37);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.Size = new System.Drawing.Size(740, 247);
            this.dataGridView1.TabIndex = 11;
            // 
            // btn_Logout
            // 
            this.btn_Logout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_Logout.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_Logout.ForeColor = System.Drawing.Color.Black;
            this.btn_Logout.Location = new System.Drawing.Point(981, 4);
            this.btn_Logout.Margin = new System.Windows.Forms.Padding(4);
            this.btn_Logout.Name = "btn_Logout";
            this.btn_Logout.Size = new System.Drawing.Size(50, 45);
            this.btn_Logout.TabIndex = 10;
            this.btn_Logout.Text = "X";
            this.btn_Logout.UseVisualStyleBackColor = false;
            this.btn_Logout.Click += new System.EventHandler(this.btn_Logout_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.PowderBlue;
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.btn_AcceptQuantity);
            this.panel2.Controls.Add(this.txt_NameAssortmentStock);
            this.panel2.Controls.Add(this.txt_AssortmentActuallyQuantity);
            this.panel2.Controls.Add(this.txt_AssortmentSystemQuantity);
            this.panel2.Controls.Add(this.dataGridView2);
            this.panel2.Location = new System.Drawing.Point(16, 338);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1035, 209);
            this.panel2.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label7.Location = new System.Drawing.Point(771, 116);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(179, 18);
            this.label7.TabIndex = 21;
            this.label7.Text = "Stan liczebny faktyczny:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label6.Location = new System.Drawing.Point(771, 68);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(191, 18);
            this.label6.TabIndex = 20;
            this.label6.Text = "Stan liczebny systemowy:";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label5.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label5.Location = new System.Drawing.Point(771, 20);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 18);
            this.label5.TabIndex = 17;
            this.label5.Text = "Nazwa:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.PowderBlue;
            this.label4.Font = new System.Drawing.Font("MS Reference Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.ForeColor = System.Drawing.SystemColors.Desktop;
            this.label4.Location = new System.Drawing.Point(20, 20);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(165, 22);
            this.label4.TabIndex = 17;
            this.label4.Text = "Inwentaryzacja:";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // btn_AcceptQuantity
            // 
            this.btn_AcceptQuantity.BackColor = System.Drawing.Color.Silver;
            this.btn_AcceptQuantity.Font = new System.Drawing.Font("MS Reference Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_AcceptQuantity.Location = new System.Drawing.Point(814, 168);
            this.btn_AcceptQuantity.Margin = new System.Windows.Forms.Padding(4);
            this.btn_AcceptQuantity.Name = "btn_AcceptQuantity";
            this.btn_AcceptQuantity.Size = new System.Drawing.Size(170, 30);
            this.btn_AcceptQuantity.TabIndex = 17;
            this.btn_AcceptQuantity.Text = "Zatwierdź";
            this.btn_AcceptQuantity.UseVisualStyleBackColor = false;
            // 
            // txt_NameAssortmentStock
            // 
            this.txt_NameAssortmentStock.Location = new System.Drawing.Point(771, 42);
            this.txt_NameAssortmentStock.Margin = new System.Windows.Forms.Padding(4);
            this.txt_NameAssortmentStock.Name = "txt_NameAssortmentStock";
            this.txt_NameAssortmentStock.ReadOnly = true;
            this.txt_NameAssortmentStock.Size = new System.Drawing.Size(253, 22);
            this.txt_NameAssortmentStock.TabIndex = 19;
            // 
            // txt_AssortmentActuallyQuantity
            // 
            this.txt_AssortmentActuallyQuantity.Location = new System.Drawing.Point(771, 138);
            this.txt_AssortmentActuallyQuantity.Margin = new System.Windows.Forms.Padding(4);
            this.txt_AssortmentActuallyQuantity.Name = "txt_AssortmentActuallyQuantity";
            this.txt_AssortmentActuallyQuantity.Size = new System.Drawing.Size(253, 22);
            this.txt_AssortmentActuallyQuantity.TabIndex = 18;
            // 
            // txt_AssortmentSystemQuantity
            // 
            this.txt_AssortmentSystemQuantity.Location = new System.Drawing.Point(771, 90);
            this.txt_AssortmentSystemQuantity.Margin = new System.Windows.Forms.Padding(4);
            this.txt_AssortmentSystemQuantity.Name = "txt_AssortmentSystemQuantity";
            this.txt_AssortmentSystemQuantity.Size = new System.Drawing.Size(253, 22);
            this.txt_AssortmentSystemQuantity.TabIndex = 17;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(24, 46);
            this.dataGridView2.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersWidth = 51;
            this.dataGridView2.Size = new System.Drawing.Size(740, 148);
            this.dataGridView2.TabIndex = 0;
            // 
            // Warehouseman
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MediumOrchid;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Warehouseman";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Warehouseman";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btn_Logout;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txt_NameAssortment;
        private System.Windows.Forms.Button btn_AcceptAssortment;
        private System.Windows.Forms.TextBox txt_NrAssortment;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button btn_AcceptQuantity;
        private System.Windows.Forms.TextBox txt_NameAssortmentStock;
        private System.Windows.Forms.TextBox txt_AssortmentActuallyQuantity;
        private System.Windows.Forms.TextBox txt_AssortmentSystemQuantity;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
    }
}