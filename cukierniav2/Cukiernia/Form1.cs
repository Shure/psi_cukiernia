﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cukiernia
{
    public partial class Shop_Form : Form
    {
        public Shop_Form()
        {
            InitializeComponent();
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Shop_Form_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
            return;
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            string login = login_txtBox.Text;
            string password = password_txtBox.Text;
            if (login == "sprzedawca" && password == "sprzedawca")
            {
                this.Hide();
                SellerPanel seller = new SellerPanel();
                seller.Show();
            }
            else if (login == "cukiernik" && password == "cukiernik")
            {
                this.Hide();
                Confectioner conf = new Confectioner();
                conf.Show();
            }
            else if (login == "magazynier" && password == "magazynier")
            {
                this.Hide();
                Warehouseman war = new Warehouseman();
                war.Show();
            }
            else
                MessageBox.Show("Niepoprawne dane logowania!");

        }

        private void login_txtBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void password_txtBox_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
