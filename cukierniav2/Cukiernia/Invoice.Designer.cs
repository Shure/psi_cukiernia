﻿namespace Cukiernia
{
    partial class Invoice
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView_listOfReceipts = new System.Windows.Forms.DataGridView();
            this.dataGridView_listOfInvoices = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_AddAllPositionReceipt = new System.Windows.Forms.Button();
            this.listView_listProductsOnInvoice = new System.Windows.Forms.ListView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.dataGridView_listOfProductsOnReceipt = new System.Windows.Forms.DataGridView();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_AddInvoice = new System.Windows.Forms.Button();
            this.txt_NrReceipt = new System.Windows.Forms.TextBox();
            this.checkBox_cash = new System.Windows.Forms.CheckBox();
            this.checkBox_card = new System.Windows.Forms.CheckBox();
            this.tct_NIPinvoice = new System.Windows.Forms.TextBox();
            this.txt_NameInvoice = new System.Windows.Forms.TextBox();
            this.btn_CloseInvoice = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_listOfReceipts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_listOfInvoices)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_listOfProductsOnReceipt)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(35, 35);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(201, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Lista paragonów:";
            // 
            // dataGridView_listOfReceipts
            // 
            this.dataGridView_listOfReceipts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_listOfReceipts.Location = new System.Drawing.Point(40, 65);
            this.dataGridView_listOfReceipts.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView_listOfReceipts.Name = "dataGridView_listOfReceipts";
            this.dataGridView_listOfReceipts.RowHeadersWidth = 51;
            this.dataGridView_listOfReceipts.Size = new System.Drawing.Size(448, 142);
            this.dataGridView_listOfReceipts.TabIndex = 1;
            // 
            // dataGridView_listOfInvoices
            // 
            this.dataGridView_listOfInvoices.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_listOfInvoices.Location = new System.Drawing.Point(560, 65);
            this.dataGridView_listOfInvoices.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView_listOfInvoices.Name = "dataGridView_listOfInvoices";
            this.dataGridView_listOfInvoices.RowHeadersWidth = 51;
            this.dataGridView_listOfInvoices.Size = new System.Drawing.Size(448, 142);
            this.dataGridView_listOfInvoices.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(555, 35);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(149, 26);
            this.label2.TabIndex = 3;
            this.label2.Text = "Lista faktur:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.PaleTurquoise;
            this.panel1.Controls.Add(this.btn_AddAllPositionReceipt);
            this.panel1.Controls.Add(this.listView_listProductsOnInvoice);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.btn_AddInvoice);
            this.panel1.Controls.Add(this.txt_NrReceipt);
            this.panel1.Controls.Add(this.checkBox_cash);
            this.panel1.Controls.Add(this.checkBox_card);
            this.panel1.Controls.Add(this.tct_NIPinvoice);
            this.panel1.Controls.Add(this.txt_NameInvoice);
            this.panel1.Location = new System.Drawing.Point(40, 229);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(968, 291);
            this.panel1.TabIndex = 4;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // btn_AddAllPositionReceipt
            // 
            this.btn_AddAllPositionReceipt.BackColor = System.Drawing.Color.Silver;
            this.btn_AddAllPositionReceipt.Location = new System.Drawing.Point(608, 123);
            this.btn_AddAllPositionReceipt.Margin = new System.Windows.Forms.Padding(4);
            this.btn_AddAllPositionReceipt.Name = "btn_AddAllPositionReceipt";
            this.btn_AddAllPositionReceipt.Size = new System.Drawing.Size(250, 30);
            this.btn_AddAllPositionReceipt.TabIndex = 12;
            this.btn_AddAllPositionReceipt.Text = "Dodaj wszystkie pozycje z paragonu";
            this.btn_AddAllPositionReceipt.UseVisualStyleBackColor = false;
            // 
            // listView_listProductsOnInvoice
            // 
            this.listView_listProductsOnInvoice.HideSelection = false;
            this.listView_listProductsOnInvoice.Location = new System.Drawing.Point(520, 161);
            this.listView_listProductsOnInvoice.Margin = new System.Windows.Forms.Padding(4);
            this.listView_listProductsOnInvoice.Name = "listView_listProductsOnInvoice";
            this.listView_listProductsOnInvoice.Size = new System.Drawing.Size(433, 82);
            this.listView_listProductsOnInvoice.TabIndex = 11;
            this.listView_listProductsOnInvoice.UseCompatibleStateImageBehavior = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.IndianRed;
            this.panel2.Controls.Add(this.dataGridView_listOfProductsOnReceipt);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Location = new System.Drawing.Point(520, 14);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(437, 103);
            this.panel2.TabIndex = 10;
            // 
            // dataGridView_listOfProductsOnReceipt
            // 
            this.dataGridView_listOfProductsOnReceipt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_listOfProductsOnReceipt.Location = new System.Drawing.Point(5, 30);
            this.dataGridView_listOfProductsOnReceipt.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView_listOfProductsOnReceipt.Name = "dataGridView_listOfProductsOnReceipt";
            this.dataGridView_listOfProductsOnReceipt.RowHeadersWidth = 51;
            this.dataGridView_listOfProductsOnReceipt.Size = new System.Drawing.Size(428, 71);
            this.dataGridView_listOfProductsOnReceipt.TabIndex = 1;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.Info;
            this.label7.Location = new System.Drawing.Point(4, 4);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(274, 20);
            this.label7.TabIndex = 0;
            this.label7.Text = "Pozycje, które mają być na fakturze";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(14, 116);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(101, 20);
            this.label6.TabIndex = 9;
            this.label6.Text = "Nr paragonu";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(14, 164);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Forma zapłaty";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(14, 66);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(36, 20);
            this.label4.TabIndex = 7;
            this.label4.Text = "NIP";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(14, 18);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Nazwa";
            // 
            // btn_AddInvoice
            // 
            this.btn_AddInvoice.BackColor = System.Drawing.Color.Silver;
            this.btn_AddInvoice.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btn_AddInvoice.Location = new System.Drawing.Point(608, 251);
            this.btn_AddInvoice.Margin = new System.Windows.Forms.Padding(4);
            this.btn_AddInvoice.Name = "btn_AddInvoice";
            this.btn_AddInvoice.Size = new System.Drawing.Size(250, 30);
            this.btn_AddInvoice.TabIndex = 5;
            this.btn_AddInvoice.Text = "Wystaw fakturę do paragonu";
            this.btn_AddInvoice.UseVisualStyleBackColor = false;
            // 
            // txt_NrReceipt
            // 
            this.txt_NrReceipt.Location = new System.Drawing.Point(131, 116);
            this.txt_NrReceipt.Margin = new System.Windows.Forms.Padding(4);
            this.txt_NrReceipt.Name = "txt_NrReceipt";
            this.txt_NrReceipt.Size = new System.Drawing.Size(368, 22);
            this.txt_NrReceipt.TabIndex = 4;
            // 
            // checkBox_cash
            // 
            this.checkBox_cash.AutoSize = true;
            this.checkBox_cash.Location = new System.Drawing.Point(131, 196);
            this.checkBox_cash.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox_cash.Name = "checkBox_cash";
            this.checkBox_cash.Size = new System.Drawing.Size(85, 21);
            this.checkBox_cash.TabIndex = 3;
            this.checkBox_cash.Text = "Gotówka";
            this.checkBox_cash.UseVisualStyleBackColor = true;
            // 
            // checkBox_card
            // 
            this.checkBox_card.AutoSize = true;
            this.checkBox_card.Location = new System.Drawing.Point(131, 166);
            this.checkBox_card.Margin = new System.Windows.Forms.Padding(4);
            this.checkBox_card.Name = "checkBox_card";
            this.checkBox_card.Size = new System.Drawing.Size(124, 21);
            this.checkBox_card.TabIndex = 2;
            this.checkBox_card.Text = "Karta płatnicza";
            this.checkBox_card.UseVisualStyleBackColor = true;
            // 
            // tct_NIPinvoice
            // 
            this.tct_NIPinvoice.Location = new System.Drawing.Point(131, 66);
            this.tct_NIPinvoice.Margin = new System.Windows.Forms.Padding(4);
            this.tct_NIPinvoice.Name = "tct_NIPinvoice";
            this.tct_NIPinvoice.Size = new System.Drawing.Size(368, 22);
            this.tct_NIPinvoice.TabIndex = 1;
            // 
            // txt_NameInvoice
            // 
            this.txt_NameInvoice.Location = new System.Drawing.Point(131, 16);
            this.txt_NameInvoice.Margin = new System.Windows.Forms.Padding(4);
            this.txt_NameInvoice.Name = "txt_NameInvoice";
            this.txt_NameInvoice.Size = new System.Drawing.Size(368, 22);
            this.txt_NameInvoice.TabIndex = 0;
            // 
            // btn_CloseInvoice
            // 
            this.btn_CloseInvoice.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.btn_CloseInvoice.Font = new System.Drawing.Font("MS Reference Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_CloseInvoice.Location = new System.Drawing.Point(1014, 3);
            this.btn_CloseInvoice.Margin = new System.Windows.Forms.Padding(4);
            this.btn_CloseInvoice.Name = "btn_CloseInvoice";
            this.btn_CloseInvoice.Size = new System.Drawing.Size(50, 45);
            this.btn_CloseInvoice.TabIndex = 5;
            this.btn_CloseInvoice.Text = "X";
            this.btn_CloseInvoice.UseVisualStyleBackColor = false;
            this.btn_CloseInvoice.Click += new System.EventHandler(this.button2_Click);
            // 
            // Invoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(1067, 554);
            this.ControlBox = false;
            this.Controls.Add(this.btn_CloseInvoice);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dataGridView_listOfInvoices);
            this.Controls.Add(this.dataGridView_listOfReceipts);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Invoice";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Faktury";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_listOfReceipts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_listOfInvoices)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_listOfProductsOnReceipt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dataGridView_listOfReceipts;
        private System.Windows.Forms.DataGridView dataGridView_listOfInvoices;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btn_AddInvoice;
        private System.Windows.Forms.TextBox txt_NrReceipt;
        private System.Windows.Forms.CheckBox checkBox_cash;
        private System.Windows.Forms.CheckBox checkBox_card;
        private System.Windows.Forms.TextBox tct_NIPinvoice;
        private System.Windows.Forms.TextBox txt_NameInvoice;
        private System.Windows.Forms.ListView listView_listProductsOnInvoice;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.DataGridView dataGridView_listOfProductsOnReceipt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btn_CloseInvoice;
        private System.Windows.Forms.Button btn_AddAllPositionReceipt;
    }
}