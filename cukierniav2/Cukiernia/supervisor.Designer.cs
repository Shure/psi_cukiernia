﻿namespace Cukiernia
{
    partial class supervisor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_employees = new System.Windows.Forms.Button();
            this.btn_invoices = new System.Windows.Forms.Button();
            this.btn_request = new System.Windows.Forms.Button();
            this.btn_orders = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btn_raports = new System.Windows.Forms.Button();
            this.btn_sales = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_employees
            // 
            this.btn_employees.BackColor = System.Drawing.Color.Peru;
            this.btn_employees.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_employees.Location = new System.Drawing.Point(24, 138);
            this.btn_employees.Name = "btn_employees";
            this.btn_employees.Size = new System.Drawing.Size(250, 60);
            this.btn_employees.TabIndex = 0;
            this.btn_employees.Text = "Pracownicy";
            this.btn_employees.UseMnemonic = false;
            this.btn_employees.UseVisualStyleBackColor = false;
            this.btn_employees.Click += new System.EventHandler(this.btn_employees_Click);
            // 
            // btn_invoices
            // 
            this.btn_invoices.BackColor = System.Drawing.Color.Peru;
            this.btn_invoices.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_invoices.Location = new System.Drawing.Point(24, 40);
            this.btn_invoices.Name = "btn_invoices";
            this.btn_invoices.Size = new System.Drawing.Size(250, 60);
            this.btn_invoices.TabIndex = 1;
            this.btn_invoices.Text = "Faktury";
            this.btn_invoices.UseVisualStyleBackColor = false;
            this.btn_invoices.Click += new System.EventHandler(this.btn_invoices_Click);
            // 
            // btn_request
            // 
            this.btn_request.BackColor = System.Drawing.Color.Peru;
            this.btn_request.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_request.Location = new System.Drawing.Point(36, 40);
            this.btn_request.Name = "btn_request";
            this.btn_request.Size = new System.Drawing.Size(250, 60);
            this.btn_request.TabIndex = 2;
            this.btn_request.Text = "Zapotrzebowanie";
            this.btn_request.UseVisualStyleBackColor = false;
            this.btn_request.Click += new System.EventHandler(this.btn_request_Click);
            // 
            // btn_orders
            // 
            this.btn_orders.BackColor = System.Drawing.Color.Peru;
            this.btn_orders.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_orders.Location = new System.Drawing.Point(24, 246);
            this.btn_orders.Name = "btn_orders";
            this.btn_orders.Size = new System.Drawing.Size(250, 60);
            this.btn_orders.TabIndex = 3;
            this.btn_orders.Text = "Zamówienia";
            this.btn_orders.UseVisualStyleBackColor = false;
            this.btn_orders.Click += new System.EventHandler(this.btn_orders_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.YellowGreen;
            this.pictureBox1.Location = new System.Drawing.Point(12, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(760, 387);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // btn_raports
            // 
            this.btn_raports.BackColor = System.Drawing.Color.Peru;
            this.btn_raports.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_raports.Location = new System.Drawing.Point(36, 138);
            this.btn_raports.Name = "btn_raports";
            this.btn_raports.Size = new System.Drawing.Size(250, 60);
            this.btn_raports.TabIndex = 5;
            this.btn_raports.Text = "Raporty";
            this.btn_raports.UseVisualStyleBackColor = false;
            this.btn_raports.Click += new System.EventHandler(this.btn_raports_Click);
            // 
            // btn_sales
            // 
            this.btn_sales.BackColor = System.Drawing.Color.Peru;
            this.btn_sales.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btn_sales.Location = new System.Drawing.Point(36, 246);
            this.btn_sales.Name = "btn_sales";
            this.btn_sales.Size = new System.Drawing.Size(250, 60);
            this.btn_sales.TabIndex = 6;
            this.btn_sales.Text = "Sprzedaż";
            this.btn_sales.UseVisualStyleBackColor = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Bisque;
            this.panel2.Controls.Add(this.btn_invoices);
            this.panel2.Controls.Add(this.btn_employees);
            this.panel2.Controls.Add(this.btn_orders);
            this.panel2.Location = new System.Drawing.Point(49, 34);
            this.panel2.Margin = new System.Windows.Forms.Padding(4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(311, 344);
            this.panel2.TabIndex = 18;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Bisque;
            this.panel1.Controls.Add(this.btn_request);
            this.panel1.Controls.Add(this.btn_raports);
            this.panel1.Controls.Add(this.btn_sales);
            this.panel1.Location = new System.Drawing.Point(408, 34);
            this.panel1.Margin = new System.Windows.Forms.Padding(4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(326, 344);
            this.panel1.TabIndex = 19;
            // 
            // supervisor
            // 
            this.BackColor = System.Drawing.Color.Khaki;
            this.ClientSize = new System.Drawing.Size(784, 411);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "supervisor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button btn_employees;
        private System.Windows.Forms.Button btn_invoices;
        private System.Windows.Forms.Button btn_request;
        private System.Windows.Forms.Button btn_orders;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btn_raports;
        private System.Windows.Forms.Button btn_sales;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
    }
}